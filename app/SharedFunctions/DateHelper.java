package SharedFunctions;

import org.joda.time.LocalDate;

/**
 * Created by wale on 6/7/15.
 */
public class DateHelper {

    public static boolean dateWithinRange(LocalDate startDate, LocalDate endDate){

        LocalDate currentDate = new LocalDate();

        if(!currentDate.isBefore(startDate) && !currentDate.isAfter(endDate)){
            return true;
        }

        return false;
    }
}

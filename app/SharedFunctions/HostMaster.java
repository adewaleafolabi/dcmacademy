package SharedFunctions;

import models.*;
import play.Logger;
import play.cache.Cache;

import static play.mvc.Http.Context.Implicit.session;

/**
 * Created by wale on 2/19/15.
 */
public class HostMaster {

    private User currentUser;

    private Student currentStudent;

    private Lecturer currentLecturer;

    private AcademicPeriod currentAcademicPeriod;

    public AcademicPeriod getCurrentAcademicPeriod(){

        return currentAcademicPeriod;
    }


    public Student getCurrentStudent(){

        String username = session().get("auth_user_name");

        Logger.debug("FOUND USERNAME:" + username);

        currentStudent = null;

        if (username == null) {

            Logger.info("No student found in session");


        } else {


            Logger.debug("In HM doing student lookup with key " + username + "_auth_student");

            currentStudent = (Student) Cache.get(username + "_auth_student");


            if (currentStudent == null) {

                Logger.info("No user found in cache");
            }

            //Lookup the db find user by username

            Student studentFromDB = Student.find.where().eq("user.username",username).findUnique();


            if (studentFromDB == null) {

                Logger.error("Student with username { " + username + " } not found in DB");

                Logger.info("Clearing session");

                session().clear();


            } else {

                //Set currentUser to one found in the DB

                currentStudent = studentFromDB;

                Cache.set(username + "_auth_student", currentStudent);

                Logger.debug("In HM saving found student to cache with key " + username + "_auth_student");


            }


        }


        return currentStudent;
    }

    public Lecturer getCurrentLecturer(){

        String username = session().get("auth_user_name");

        Logger.debug("FOUND USERNAME:" + username);

        currentLecturer = null;

        if (username == null) {

            Logger.info("No lecturer found in session");


        } else {


            Logger.debug("In HM doing lecturer lookup with key " + username + "_auth_lecturer");

            currentLecturer = (Lecturer) Cache.get(username + "_auth_lecturer");


            if (currentLecturer == null) {

                Logger.info("No user found in cache");
            }

            //Lookup the db find user by username

            Lecturer lecturerFromDB = Lecturer.find.where().eq("user.username",username).findUnique();


            if (lecturerFromDB == null) {

                Logger.error("Lecturer with username { " + username + " } not found in DB");

                Logger.info("Clearing session");

                session().clear();


            } else {

                //Set currentUser to one found in the DB

                currentLecturer = lecturerFromDB;

                Cache.set(username + "_auth_lecturer", currentLecturer);

                Logger.debug("In HM saving found lecturer to cache with key " + username + "_auth_lecturer");


            }


        }


        return currentLecturer;
    }





    public User getCurrentUser() {

        String username = session().get("auth_user_name");

        Logger.debug("FOUND USERNAME:" + username);

        currentUser = null;

        if (username == null) {

            Logger.info("No user found in session");


        } else {


            Logger.debug("In HM doing lookup with key " + username + "_auth_user");

            currentUser = (User) Cache.get(username + "_auth_user");


            if (currentUser == null) {

                Logger.info("No user found in cache");
            }

            //Lookup the db find user by username

            User userFromDB = User.findByUserName(username);


            if (userFromDB == null) {

                Logger.error("User with username { " + username + " } not found in DB");

                Logger.info("Clearing session");

                session().clear();


            } else {

                //Set currentUser to one found in the DB

                currentUser = userFromDB;

                Cache.set(username + "_auth_user", currentUser);

                Logger.debug("In HM saving found user to cache with key " + username + "_auth_user");


            }


        }


        return currentUser;
    }

}

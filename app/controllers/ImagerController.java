package controllers;

import SharedFunctions.ControllerShared;
import play.Play;
import play.mvc.Result;
import models.Repo;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wale on 3/6/15.
 */
public class ImagerController extends ControllerShared{




    public Result index(){
        return ok();
    }

    public Result lecturer(String filename){

        return fileLoader(Repo.LECTURER, filename);

    }

    public Result course(String filename){

        return fileLoader(Repo.COURSE, filename);

    }


    private Result fileLoader(Repo repo, String filename){

        File image = new File(repo.getRepoPath(), filename);

        if (image.exists()) {

            return ok(image);

        }else{

            String fileName =repo.name()+"_NOT_FOUND.jpg";

            image = new File(Repo.GENERAL.getRepoPath(), fileName);

            return ok(image);
        }

    }



}

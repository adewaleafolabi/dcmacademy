package controllers;

import models.Faculty;
import models.Faculty;
import models.Lecturer;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * Created by wale on 2/25/15.
 */
public class FacultyController extends Controller {

    public static Promise<Result> view(final long id) {

        return Faculty.find(id).map(new F.Function<Faculty, Result>() {
            @Override
            public Result apply(Faculty faculty) throws Throwable {

                if(faculty == null){
                    flash("danger", "The specified faculty was not found.");

                    return redirect(routes.Application.viewSite());
                }
                return ok(views.html.faculty.show.render(faculty));
            }
        });
    }


}

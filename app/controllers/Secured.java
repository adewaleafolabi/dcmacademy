package controllers;


import play.i18n.Messages;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import static play.mvc.Controller.*;

/**
 * User: yesnault
 * Date: 22/01/12
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Http.Context ctx) {
        return ctx.session().get("auth_user_name");
    }

    @Override
    public Result onUnauthorized(Http.Context ctx) {
        flash("danger", "kindly login first");
        play.Logger.debug("URL {}", ctx.request().uri().toString());
        session("dest_url", ctx.request().uri().toString());

        ctx.session().put("dest_url",ctx.request().uri().toString() );

        play.Logger.debug("SessionDirect{}",session("dest_url"));

        Http.Context.current().session().put("dest_url", ctx.request().uri().toString());



        return redirect(routes.Application.showLogin());
    }
}
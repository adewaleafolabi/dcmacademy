package controllers;

import SharedFunctions.ControllerShared;
import models.Role;
import models.Student;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by wale on 5/11/15.
 */
@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.STUDENT)
public class ResultController extends ControllerShared {



    public Result index(){

        Student student = hm.getCurrentStudent();

        return ok(views.html.student.results.render(student));


    }

}

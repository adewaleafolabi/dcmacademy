package controllers;

import SharedFunctions.ControllerShared;
import SharedFunctions.HostMaster;
import models.AuditTrails.Login;
import models.AuditTrails.LoginResult;
import models.Course;
import models.FeaturedCourse;
import models.Forms.LoginForm;
import models.Student;
import models.User;
import play.*;
import play.cache.Cache;
import play.data.Form;
import play.libs.F;
import play.libs.Json;
import play.mvc.*;
import static play.mvc.Http.Context.Implicit.session;
import views.html.*;

public class Application extends ControllerShared {



    public Result GO_HOME = redirect(routes.Application.index());

    public Result GO_DASHBOARD = redirect(routes.Dashboard.index());




    public Result index() {


        /*if(currentUser !=null){

            if( currentUser != null &&  currentUser.isEnabled){

                return GO_DASHBOARD;

            }else{

                Logger.info("User account not found or disabled");

                Logger.debug("Clearing invalid session credentials");

                session().clear();
            }
        }*/



        return ok(home.render(FeaturedCourse.find.setMaxRows(6).findList()));

    }


    public Result viewSite(){

        return ok(home.render(FeaturedCourse.find.setMaxRows(6).findList()));
    }


    public Result showLogin(){
        Form<LoginForm> loginForm = Form.form(LoginForm.class);


        return ok(login.render(loginForm));

    }

    public Result login(){

        Form<LoginForm> loginForm = Form.form(LoginForm.class).bindFromRequest();


        if(loginForm.hasErrors()){
            play.Logger.error("Form has error -- {}", loginForm.errorsAsJson().toString());

            return badRequest(views.html.login.render(loginForm));
        }


        session("auth_user_name",loginForm.get().username);

        String destinationUrl = session().get("dest_url");

        play.Logger.debug("From controller ");

        play.Logger.debug("SessionDirect{}",session("dest_url"));


        if(destinationUrl == null){
            return GO_DASHBOARD;
        }else{
            session("dest_url","");
            return redirect(destinationUrl);
        }



    }


    public Result logout() {



        Logger.debug("Logout initiated");

        if( currentUser == null){

            Logger.info("No user currently logged in");

        }else{

            Cache.remove( currentUser.username + "_auth_user");



            session().clear();

            Logger.info("Cache and Session cleared");

            try{
                Login loginTrail=  new Login(LoginResult.LOGOUT_OK,request().remoteAddress(),currentUser);

                play.Logger.info("LoginTrail {}",loginTrail.toString());

                loginTrail.save();

            }catch(Exception e){

                play.Logger.error("failed to save audit trail {}",e);

            }

        }

        Logger.info("user ended session ");

        flash("success", "Logout Successful");


        return GO_HOME;

    }


    public Result whoWeAre(){

        return ok(whoweare.render());
    }



}

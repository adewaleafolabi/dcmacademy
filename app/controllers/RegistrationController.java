package controllers;

import models.BioData;
import models.Forms.Registration;
import models.Role;
import models.Student;
import models.User;
import play.Play;
import org.apache.commons.mail.EmailException;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.Hash;
import utils.Mail;
import views.html.register;
import views.html.confirm;
import play.Logger;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
import java.net.URL;
import java.util.UUID;
import play.i18n.Messages;
import utils.AppException;

/**
 * Created by wale on 2/16/15.
 */
public class RegistrationController extends Controller{


    public Result index(){

        Form<Registration> registrationForm = Form.form(Registration.class);

        return ok(register.render(registrationForm));
    }


    public Result save(){

        Form<Registration> registrationForm = Form.form(Registration.class).bindFromRequest();

        if(registrationForm.hasErrors()){

            return badRequest(register.render(registrationForm));
        }

        Registration registration = registrationForm.get();


        try{
            User user = new User();
            user.username = registration.email;
            user.bioData = new BioData();
            user.bioData.user = user;
            user.bioData.firstName= registration.firstName;
            user.bioData.middleName= registration.middleName;
            user.bioData.lastName= registration.lastName;

            user.role= Role.STUDENT;

            user.passwordHash = Hash.createPassword(registration.password);
            
            user.confirmationToken = UUID.randomUUID().toString();

            Student student = new Student();
            student.user = user;


            user.student = student;

            user.save();



            sendMailAskForConfirmation(user);
            
            return ok(views.html.successreg.render());



        }catch(org.apache.commons.mail.EmailException e) {
            Logger.debug("Registration.save Cannot send email", e);

            flash("error", "Error sending email");

        } catch (Exception e) {

            Logger.error("Registration.save error", e);

            flash("error", "Technical error");
        }
        return badRequest(register.render(registrationForm));

    }


    /**
     * Valid an account with the url in the confirm mail.
     *
     * @param token a token attached to the user we're confirming.
     * @return Confirmationpage
     */
    public static Result confirm(String token) {
        User user = User.find.where().eq("confirmationToken",token).findUnique();

        if (user == null) {
            flash("error", "Invalid email address");
            return badRequest(confirm.render());
        }

        if (user.isValidated) {
            flash("error", Messages.get("error.account.already.validated"));
            return badRequest(confirm.render());
        }

        try {
            if (User.confirm(user)) {
                sendMailConfirmation(user);
                flash("success", Messages.get("account.successfully.validated"));
                return ok(confirm.render());
            } else {
                Logger.debug("Signup.confirm cannot confirm user");
                flash("error", Messages.get("error.confirm"));
                return badRequest(confirm.render());
            }
        } catch (AppException e) {
            Logger.error("Cannot signup", e);
            flash("error", Messages.get("error.technical"));
        } catch (EmailException e) {
            Logger.debug("Cannot send email", e);
            flash("error", Messages.get("error.sending.confirm.email"));
        }
        return badRequest(confirm.render());
    }


    /**
     * Send the confirm mail.
     *
     * @param user user created
     * @throws EmailException Exception when sending mail
     */
    private static void sendMailConfirmation(User user) throws EmailException {
        String subject = "Welcome to DCM Academy";
        String message = "Dear "+user.bioData.firstName+",<br/>Thank you for confirming your email. Your account has been activated.<p>Regards,<br/>DCM Academy.</p>";
        Mail.Envelop envelop = new Mail.Envelop(subject, message, user.username);
        Mail.sendMail(envelop);
    }


    /**
     * Send the welcome Email with the link to confirm.
     *
     * @param user user created
     * @throws EmailException Exception when sending mail
     */
    private static void sendMailAskForConfirmation(User user) throws EmailException, MalformedURLException {
        String subject = "DCMA Academy - Confirm your email";

        String urlString = "http://" + Play.application().configuration().getString("server.hostname");

        urlString += "/confirm/" + user.confirmationToken;


        URL url = new URL(urlString); // validate the URL, will throw an exception if bad.

        String message = "Welcome to DCM Academy. <br> Click on this link : "+url.toString()+" to confirm your email";

        Mail.Envelop envelop = new Mail.Envelop(subject, message, user.username);

        Mail.sendMail(envelop);
    }


}

package controllers;

import play.mvc.Controller;
import play.mvc.Result;

/**
 * Created by wale on 2/25/15.
 */
public class SetupController extends Controller {
    public static Result index(){

        return ok(views.html.dashboard.admin.render());
    }
}

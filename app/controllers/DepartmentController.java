package controllers;

import com.avaje.ebean.config.dbplatform.MySqlBlob;
import models.Department;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

/**
 * Created by wale on 2/25/15.
 */
public class DepartmentController extends Controller {

    public static Promise<Result> view(final long id) {
        return Department.find(id).map(new F.Function<Department, Result>() {
            @Override
            public Result apply(Department department) throws Throwable {

                if(department == null){
                    flash("danger", "The specified department was not found.");

                    return redirect(routes.Application.viewSite());
                }
                return ok(views.html.department.show.render(department));
            }
        });
    }
}

package controllers;

import SharedFunctions.ControllerShared;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import models.*;
import models.Forms.CourseCreationForm;
import models.Forms.CourseSearchForm;
import org.apache.commons.lang3.StringUtils;
import play.Play;
import play.cache.Cache;
import play.cache.Cached;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import org.apache.commons.io.FileUtils;
import play.mvc.Security;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by wale on 2/16/15.
 */
public class CourseController extends ControllerShared{



    public Result index(){

        return page(1);
    }

    public Result page(Integer page) {

        String uuid = session("uuid");
        if (uuid == null) {
            uuid = java.util.UUID.randomUUID().toString();
            session("uuid", uuid);
        }

        PagingList<Course> pagingList = null;

        pagingList = (PagingList<Course>) Cache.get(uuid + "pagingList");

        if (pagingList == null) {

            pagingList = Course.find.findPagingList(4);

        }

        // -1 because page starts on 0
        Page<Course> currentPage = pagingList.getPage(page - 1);
        // pagineList save in cache with unique uuid from session
        Cache.set(uuid + "pagingList", pagingList);

        List<Course> courses = currentPage.getList();

        Integer totalPageCount = pagingList.getTotalPageCount();

        return ok(views.html.course.paginated.render(courses,page,totalPageCount,AcademicPeriod.find.byId(1L)));
    }


    public Result search(){

        Form<CourseSearchForm> courseSearchForm =  Form.form(CourseSearchForm.class).bindFromRequest();

        CourseSearchForm courseSearchFormData = courseSearchForm.get();




        return pageMaker(courseSearchFormData.page, courseSearchFormData);


        
    }
    
    private Result pageMaker(Integer page, CourseSearchForm courseSearchForm ){

        String uuid = session("uuid");
        if (uuid == null) {
            uuid = java.util.UUID.randomUUID().toString();
            session("uuid", uuid);
        }

        PagingList<Course> pagingList = null;

        CourseSearchForm CourseSearchFormCache = (CourseSearchForm)Cache.get("uuid"+courseSearchForm);

        if(courseSearchForm.equals(CourseSearchFormCache)){

            pagingList = (PagingList<Course>) Cache.get(uuid + "pagingListSearch");

        }else{
            pagingList = null;
        }


        if (pagingList == null) {

            if(courseSearchForm !=null){
                String title = courseSearchForm.title;


                if(courseSearchForm.departments.size() > 0){
                    pagingList =  Course.find.where().and(Expr.in("department.id",courseSearchForm.departments),Expr.ilike("title","%"+title+"%")).findPagingList(4);
                }else{
                    pagingList =  Course.find.where().ilike("title", "%" + title + "%").findPagingList(4);
                }

            }else {

                pagingList = Course.find.findPagingList(4);
            }

        }

        // -1 because page starts on 0
        Page<Course> currentPage = pagingList.getPage(page - 1);
        // pagineList save in cache with unique uuid from session
        Cache.set(uuid + "pagingListSearch", pagingList);

        List<Course> courses = currentPage.getList();

        Integer totalPageCount = pagingList.getTotalPageCount();

        return ok(views.html.course.paginatedsearch.render(courses,courseSearchForm,page,totalPageCount));


    }










    public Promise<Result> view(final long id) {

        return Course.find(id).map(new F.Function<Course, Result>() {
            @Override
            public Result apply(Course course) throws Throwable {
                if(course == null){
                    flash("danger", "The course was not found");
                    return redirect(routes.CourseController.index());
                }
                if(hm.getCurrentStudent() !=null) {
                    

                    play.Logger.debug("Current student{}",hm.getCurrentStudent());

                    if(!hm.getCurrentStudent().recentlyViewedCourses.contains(course)){

                        play.Logger.debug("SizeCurrentlyViewed {}",hm.getCurrentStudent().recentlyViewedCourses.size());

                        if(hm.getCurrentStudent().recentlyViewedCourses.size()>1){
                            hm.getCurrentStudent().recentlyViewedCourses.remove(0);
                        }

                        hm.getCurrentStudent().recentlyViewedCourses.add(course);


                    }

                    try{
                        hm.getCurrentStudent().update();

                        play.Logger.debug("SizeCurrentlyViewed {}",hm.getCurrentStudent().recentlyViewedCourses.size());

                    }catch(Exception exp){
                        play.Logger.error("tried saving recent courses viewed {}" , exp);
                    }



                }
                List<Course> relatedCourses = Course.find.where().eq("department.id",course.department.id).ne("id",course.id).setMaxRows(5).findList();

                 return ok(views.html.course.show.render(course,relatedCourses));
            }
        });

    }



    @Security.Authenticated(Secured.class)
    @RoleSecured(role = Role.STUDENT)
    public Promise<Result> enroll(final long id){



        return Course.find(id).map(new F.Function<Course, Result>() {
            @Override
            public Result apply(Course course) throws Throwable {

                if(course == null){
                    flash("danger", "The specified course does not exists");

                    return redirect(routes.CourseController.index());
                }

                Student s = Student.find.where().eq("user.id",currentUser.id).findUnique();

                if(s == null){
                    flash("danger","Your current profile does not permit this action");

                    return redirect(routes.Dashboard.index());
                }

                /*s.courses.add(course);

                try
                {
                    s.update();
                }
                catch (Exception e){

                    play.Logger.error("Error updating student's courses {}",e);

                    flash("danger","Technical error. Please try again or contact the administrator for assistance");

                    return redirect(routes.CourseController.index());
                }

                flash("success","You have successfully registered for this course");

                List<Course> relatedCourses = Course.find.where().eq("department.id",course.department.id).ne("id",course.id).setMaxRows(5).findList();


                return ok(views.html.course.show.render(course,relatedCourses));
                */

                return ok(views.html.course.enroll.render(course));
            }
        });
    }



    public Result completeEnrolment(){

        DynamicForm dynamicForm = Form.form().bindFromRequest();


        long courseID = Long.parseLong(dynamicForm.get("courseID"));

        long academicPeriodID = Long.parseLong(dynamicForm.get("academicPeriodID"));


        AcademicPeriod academicPeriod = AcademicPeriod.find.where().eq("id",academicPeriodID).eq("availableCourses.id",courseID).findUnique();

       if(academicPeriod == null){

           flash("warning", "The selected session for this course is not available");

           return redirect(routes.CourseController.enroll(courseID));
       }

        CourseRegistration courseRegistration = new CourseRegistration();


        courseRegistration.status = models.Status.PENDING;

        courseRegistration.student= hm.getCurrentStudent();

        courseRegistration.course = Course.find.byId(courseID);

        courseRegistration.registrationDate = new Date();

        courseRegistration.academicPeriod = academicPeriod;


        try{
            courseRegistration.save();

        }catch (Exception exp){

            play.Logger.error("{}",exp);
        }

        flash("success","Enrollment Complete");

        return redirect(routes.CourseController.enroll(courseID));
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = Role.STUDENT)
    public Result myCourses(){

        Student s = Student.find.where().eq("user.id",currentUser.id).findUnique();

        return ok(views.html.student.mycourses.render(s));

    }


}

package controllers;


import controllers.Secured;
        import models.Token;
        import models.User;
        import play.Logger;
        import play.i18n.Messages;
        import play.mvc.Controller;
        import play.mvc.Result;
        import play.mvc.Security;
        import views.html.password;
        import java.net.MalformedURLException;

/**
 * User: yesnault
 * Date: 15/05/12
 */
@Security.Authenticated(Secured.class)
public class PasswordController extends Controller {

    /**
     * Password Page. Ask the user to change his password.
     *
     * @return index settings
     */
    public static Result index() {
        play.Logger.debug(request().username());
        return ok(password.render());
    }

    /**
     * Send a mail with the reset link.
     *
     * @return password page with flash error or success
     */
    public static Result runPassword() {
        User user = User.findByEmail(request().username());
        try {
            Token.sendMailResetPassword(user);
            flash("success", Messages.get("resetpassword.mailsent"));
            return ok(password.render());
        } catch (MalformedURLException e) {
            Logger.error("Cannot validate URL", e);
            flash("error", Messages.get("error.technical"));
        }
        return badRequest(password.render());
    }
}
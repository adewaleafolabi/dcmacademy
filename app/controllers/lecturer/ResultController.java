package controllers.lecturer;

import SharedFunctions.ControllerShared;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.*;
import controllers.routes;
import models.AcademicPeriod;
import models.Course;
import models.ExamRecord;
import models.Forms.ExamRecordForm;
import models.Student;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;

import java.util.List;

/**
 * Created by wale on 5/11/15.
 */
public class ResultController extends ControllerShared {

    public Result index(){
        List<Course> myCourses = Course.find.where().in("lecturers.id", hm.getCurrentLecturer().id).findList();

        List<AcademicPeriod> academicPeriods = AcademicPeriod.find.all();

        return ok(views.html.course.lecturerSheetPgI.render(myCourses,academicPeriods));


    }

    public Result loader(){

        DynamicForm requestData = Form.form().bindFromRequest();

        long courseID = 0;

        long academicPeriodID = 0;

        try{
            courseID = Long.parseLong(requestData.get("course"));

            academicPeriodID = Long.parseLong(requestData.get("academicPeriod"));


        }catch(Exception e){

            flash("danger","Kindly fill the required fields");

            return index();
        }



        return showSheet(courseID,academicPeriodID);
    }


    public Result showSheet(long id, long academicPeriodID){

        Course course = Course.find.where().in("lecturers.id", hm.getCurrentLecturer().id).idEq(id).findUnique();

        AcademicPeriod academicPeriod = AcademicPeriod.find.byId(academicPeriodID);

        if(course == null){

            flash("danger","The specified course does not exists");

            return index();


        }



        Form<ExamRecordForm> examRecordForm = Form.form(ExamRecordForm.class);

        return ok(views.html.course.lecturersheet.render(examRecordForm, course,academicPeriod));

    }


    public Result saveRecords(){

        String failed="";

        Form<ExamRecordForm> examRecordForm = Form.form(ExamRecordForm.class).bindFromRequest();


        if(examRecordForm.hasErrors()){

            play.Logger.error("Form has error ");

            return redirect(controllers.lecturer.routes.ResultController.index());
        }

        if(examRecordForm.get().examRecords == null){

            flash("danger","Kindly provide some data");

            return redirect(controllers.lecturer.routes.ResultController.index());

        }



        for(ExamRecord examRecord : examRecordForm.get().examRecords){

            Student s = Student.find.byId(examRecord.student.id);

            if(s == null){
                continue;
            }


            ExamRecord examRecord1 = new ExamRecord();
            examRecord1.attendanceScore = examRecord.attendanceScore;
            examRecord1.caScore = examRecord.caScore;
            examRecord1.examScore = examRecord.examScore;
            examRecord1.course = Course.find.byId(examRecord.course.id);
            examRecord1.student = s;
            examRecord1.academicPeriod = AcademicPeriod.find.byId(examRecord.academicPeriod.id);

            s.examRecords.add(examRecord1);

            try{
                s.update();
            }catch(Exception e){

               play.Logger.error("{}",e);
            }


        }
        flash("success","Exam records saved");

        return redirect(controllers.routes.Dashboard.index());
    }
}

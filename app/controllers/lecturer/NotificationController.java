package controllers.lecturer;

import SharedFunctions.ControllerShared;
import controllers.RoleSecured;
import controllers.Secured;
import models.Course;
import models.Notification;
import models.Role;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by wale on 5/6/15.
 */

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.LECTURER)
public class NotificationController extends ControllerShared {


    public Result index(){

        //play.Logger.debug("{}",Course.find.where().in("lecturers.id",currentLecturer.id).findList().size());

        return ok(views.html.notification.list.render(Notification.find.where().eq("lecturer.id",hm.getCurrentLecturer().id).findList()));


    }


    public Result create(){

        Form<Notification> notificationForm = Form.form(Notification.class);

        return ok(views.html.notification.create.render(notificationForm, hm.getCurrentLecturer().courses));

    }


    public Result save(){

        Form<Notification> notificationForm = Form.form(Notification.class).bindFromRequest();

        Result createTemplate = badRequest(views.html.notification.create.render(notificationForm, hm.getCurrentLecturer().courses));

        if(notificationForm.hasErrors()){

            play.Logger.error("Form error{}",notificationForm.errorsAsJson().asText());

            return badRequest(views.html.notification.create.render(notificationForm, hm.getCurrentLecturer().courses));
        }

        Notification notificationFormData = notificationForm.get();

        Notification notification = new Notification();
        notification.course = Course.find.byId(notificationFormData.course.id);
        notification.message = notificationFormData.message;
        notification.lecturer = hm.getCurrentLecturer();


        try{

            play.Logger.debug("", notification.toString());

            notification.save();

            play.Logger.debug("Notification saved");

        }catch (Exception exp){

            play.Logger.error("Saving notification{}",exp);

            return createTemplate;
        }


        flash("success", "Course notification has been saved");

        return redirect(routes.NotificationController.index());

    }
}

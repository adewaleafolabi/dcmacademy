package controllers.lecturer;

import SharedFunctions.ControllerShared;
import controllers.RoleSecured;
import controllers.Secured;
import models.*;
import org.apache.commons.lang3.StringUtils;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import play.twirl.api.Html;

import java.io.File;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.LECTURER)
public class CourseMaterialController extends ControllerShared {


    public  Result index() {

        play.Logger.debug("Courses {}", Course.find.where().in("lecturers.id", hm.getCurrentLecturer().id).findList().size());

        List<CourseDocumentation> courseDocumentations = CourseDocumentation.find.where().in("course.lecturers.id", hm.getCurrentLecturer().id).findList();


        return ok(views.html.course.material.list.render(courseDocumentations));
    }

    public  Result create() {

        Form<CourseDocumentation> documentationForm = Form.form(CourseDocumentation.class);

        return ok(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));
    }


    public  Result save() {

        Form<CourseDocumentation> documentationForm = Form.form(CourseDocumentation.class).bindFromRequest();

        Html FORM_TEMPLATE = views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses);

        if(documentationForm.hasErrors()){

            play.Logger.error("Form errors {}", documentationForm.errorsAsJson().asText());

            return badRequest(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));
        }



        Course course = Course.find.where().in("lecturers.id", hm.getCurrentLecturer().id).idEq(documentationForm.get().course.id).findUnique();

        if(course == null){

            flash("danger","Kindly select a valid course");

            play.Logger.error("Invalid course selected");

            return badRequest(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));
        }

        Http.MultipartFormData body = request().body().asMultipartFormData();

        if (body == null) {

            play.Logger.error("Invalid request, required is POST with enctype=multipart/form-data.");

             flash("danger", "Invalid request, required is POST with enctype=multipart/form-data.");

            return badRequest(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("file");


        if (filePart == null) {

            flash("danger","Invalid request, no file has been sent.");

            return badRequest(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));
        }

        List<String> acceptaples = Arrays.asList(new String[]{
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                "application/vnd.ms-excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "application/pdf",
                "application/msword",
                "application/vnd.ms-office",
                "application/vnd.ms-powerpoint",
                "application/wps-office.xls",
                "application/wps-office.xlsx",
                "application/wps-office.docx",
                "application/wps-office.doc",
                "application/wps-office.ppt",
                "application/wps-office.pptx"


        });

        if (!acceptaples.contains(filePart.getContentType().toLowerCase())) {

            play.Logger.error("Wrong extension {}",filePart.getContentType().toLowerCase());

            flash("danger", "Invalid request, only PDFs and Microsoft document files are allowed.");

            return badRequest(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));
        }






        try {

            String destinationFileName = UUID.randomUUID().toString()+"."+StringUtils.substringAfterLast(filePart.getFilename(),".");

            File file = filePart.getFile();

            String uploadPath = Repo.DOCUMENTS.getRepoPath();

            File destination = new File(uploadPath, destinationFileName);

            if (destination.exists()) {
                destination.renameTo(new File(uploadPath, destinationFileName + "-" + new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date())));
            }

            org.apache.commons.io.FileUtils.moveFile(file, destination);

            CourseDocumentation courseDocumentation = new CourseDocumentation();
            courseDocumentation.course = course;
            courseDocumentation.fileName = destinationFileName;
            courseDocumentation.title = documentationForm.get().title;

            courseDocumentation.save();

            flash("success","Document added successfully");

            return redirect(routes.CourseMaterialController.index());



        }
        catch(Exception e){

            play.Logger.error("{}", e);


            if(e.getClass().isInstance(SQLIntegrityConstraintViolationException.class)){
                flash ("danger", "This course already has a document with the same title");

            }else{
                flash ("danger", "A technical error occured");
            }

            return badRequest(views.html.course.material.create.render(documentationForm, hm.getCurrentLecturer().courses));

        }

    }

    public  Result edit(long id) {
        return TODO;
    }

    public  Result delete(long id) {
        return TODO;
    }



    public  Result update() {
        return TODO;
    }


}
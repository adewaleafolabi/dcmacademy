package controllers;

import SharedFunctions.HostMaster;
import play.libs.F;
import play.Logger;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

/**
 * Created by wale on 2/25/15.
 */
public class CurrentUserAction extends Action.Simple {

    HostMaster hm = new HostMaster();

    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {

        context.args.put("user",hm.getCurrentUser());

        return delegate.call(context);
    }
}

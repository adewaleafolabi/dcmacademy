package controllers;

import models.*;
import models.Forms.LecturerForm;
import play.Play;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.AppException;
import utils.Hash;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 2/16/15.
 */
public class LecturerController extends Controller {

    public static Promise<Result> index() {
        return Lecturer.find().map(new F.Function<List<Lecturer>, Result>() {
            @Override
            public Result apply(List<Lecturer> lecturers) throws Throwable {
                return ok(views.html.lecturer.list.render(lecturers));
            }
        });
    }




    public static Promise<Result> view(final long id) {

        return Lecturer.find(id).map(new F.Function<Lecturer, Result>() {
            @Override
            public Result apply(Lecturer lecturer) throws Throwable {
                return ok(views.html.lecturer.show.render(lecturer));
            }
        });


    }




}

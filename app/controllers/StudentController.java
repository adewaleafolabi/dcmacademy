package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.BioData;
import models.Forms.StudentForm;
import models.Role;
import models.Student;
import models.User;
import org.apache.commons.lang3.RandomStringUtils;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import utils.Hash;
import utils.Mail;
import views.html.student.*;

import java.util.List;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.SUPER_ADMIN)
public class StudentController extends Controller {


    public static Promise<Result> index() {
        return Student.find().map(new F.Function<List<Student>, Result>() {
            @Override
            public Result apply(List<Student> students) throws Throwable {
                return ok(views.html.student.list.render(students));
            }
        });
    }

    public static Result create() {


        Form<StudentForm> userForm = Form.form(StudentForm.class);

        return ok(views.html.student.create.render(userForm));
    }


    public static Result save() {
        Form<StudentForm> studentForm = Form.form(StudentForm.class).bindFromRequest();

        String clearPassword = RandomStringUtils.randomAlphanumeric(8);

        if(studentForm.hasErrors()){
            return badRequest(views.html.student.create.render(studentForm));
        }

        StudentForm studentFormData = studentForm.get();



        User user = new User();

        Student student = new Student();
        student.user = user;

        user.student = student;
        user.username = studentFormData.email;
        user.role = Role.STUDENT;

        user.bioData = new BioData();
        user.bioData.user= user;
        user.bioData.firstName = studentFormData.firstName;
        user.bioData.lastName = studentFormData.lastName;
        user.bioData.middleName = studentFormData.middleName;
        user.bioData.phoneNumber = studentFormData.phoneNumber;
        user.bioData.salutation = studentFormData.salutation;
        user.bioData.residentialAddress = studentFormData.residentialAddress;
        user.bioData.gender = studentFormData.gender;
        user.isEnabled = true;
        user.isValidated = true;

        try{
            user.passwordHash = Hash.createPassword(clearPassword);

            user.save();

            flash("success","Student created successfully");

            String emailPayload = views.html.mails.studentcreated.render(student,clearPassword).toString();

            String subject = "Welcome to DCM Academy";

            String message = emailPayload;

            Mail.Envelop envelop = new Mail.Envelop(subject, message, user.username);

            Mail.sendMail(envelop);




        }catch(Exception exp){

            play.Logger.error("Save new student failed",exp);

            flash("warning","Technical error occured. Please try again");

            return ok(views.html.student.create.render(studentForm));

        }



        return redirect(routes.StudentController.index());

    }

    public static Promise<Result> view(long id) {
        return Student.find(id).map(new F.Function<Student, Result>() {
            @Override
            public Result apply(Student student) throws Throwable {
                if(student ==null){
                    flash("danger","The student data was not found");

                    return redirect(routes.StudentController.index());
                }
                 return ok(views.html.student.show.render(student));
            }
        });
    }

    public static Result edit(long id) {
        Student student = Student.find.byId(id);

        if(student == null){

            flash("danger","The student was not found");

            redirect(routes.StudentController.index());
        }

        Form<StudentForm> studentForm = Form.form(StudentForm.class);

        StudentForm data = new StudentForm(student);



        return ok(views.html.student.edit.render(studentForm.fill(data)));
    }



    public static Result update() {


        Form<StudentForm> studentForm = Form.form(StudentForm.class).bindFromRequest();


        if(studentForm.hasErrors()){
            return badRequest(views.html.student.edit.render(studentForm));
        }

        StudentForm studentFormData = studentForm.get();




        Student student = Student.find.byId(studentFormData.id);

        if(student == null){

            flash("danger","The student was not found");

            redirect(routes.StudentController.index());
        }



        User user = student.user;

        user.bioData.firstName = studentFormData.firstName;
        user.bioData.lastName = studentFormData.lastName;
        user.bioData.middleName = studentFormData.middleName;
        user.bioData.phoneNumber = studentFormData.phoneNumber;
        user.bioData.salutation = studentFormData.salutation;
        user.bioData.residentialAddress = studentFormData.residentialAddress;
        user.bioData.gender = studentFormData.gender;
        user.bioData.phoneNumber = studentFormData.phoneNumber;


        try{

            user.update();

            flash("success","Student created successfully");

        }catch(Exception exp){

            play.Logger.error("Save new student failed",exp);

            flash("warning","Technical error occured. Please try again");

            return ok(views.html.student.edit.render(studentForm));

        }



        return redirect(routes.StudentController.index());
    }






    public static Promise<Result> enable(long id) {

        return Student.find(id).map(new F.Function<Student, Result>() {
            @Override
            public Result apply(Student student) throws Throwable {

                ObjectNode output = Json.newObject();

                if(student == null){
                    output.put("message","Kindly provide a valid student ID");
                    return ok(output);
                }

                student.user.isEnabled = false;

                User user = student.user;
                user.isEnabled = true;

                try{
                    user.update();

                    output.put("message","User is enabled");

                    return ok(output);
                }
                catch (Exception e){
                    play.Logger.error("Failed to enable user{}",e);

                    output.put("message", "Technical Error. Try again");

                    return ok(output);
                }
            }
        });


    }

    public static Promise<Result> disable(long id) {
        return Student.find(id).map(new F.Function<Student, Result>() {
            @Override
            public Result apply(Student student) throws Throwable {

                ObjectNode output = Json.newObject();

                if(student == null){
                    output.put("message","Kindly provide a valid student ID");
                    return ok(output);
                }

                student.user.isEnabled = false;

                User user = student.user;
                user.isEnabled = false;

                try{
                    user.update();

                    output.put("message","User is disabled");

                    return ok(output);
                }
                catch (Exception e){
                    play.Logger.error("Failed to disable user{}",e);

                    output.put("message","Technical Error. Try again");

                    return ok(output);
                }
            }
        });
    }


}

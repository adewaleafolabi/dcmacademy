package controllers;

import SharedFunctions.HostMaster;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Role;
import models.User;
import play.i18n.Messages;
import play.libs.F;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import static play.mvc.Controller.flash;

/**
 * Created by wale on 5/2/15.
 */
public class RoleAction  extends Action<RoleSecured> {
    @Override
    public F.Promise<Result> call(Http.Context context) throws Throwable {

        User user = (new HostMaster().getCurrentUser());

        Role role = configuration.role();

        play.Logger.debug("User[id={}], Role[name={}] , ",user.id,role.name());
        if(!user.role.equals(Role.SUPER_ADMIN)) {

            if (!user.role.equals(role)) {

                play.Logger.info("Role requirement failed. Required profile - {} , User profile {}", role, user.role);

                String message = "Your current profile does not permit you to carry out this task.";


                if (configuration.isJsonResponse()) {

                    ObjectNode output = Json.newObject();

                    output.put("message", message);

                    return F.Promise.<Result>pure(ok(output));
                }

                flash("warning", message);

                return F.Promise.pure(redirect(routes.Dashboard.index()));


            }
        }


        /*if(!user.role.equals(role)){

            play.Logger.info("Role requirement failed. Required profile - {} , User profile {}",role,user.role);

            String message = "Your current profile does not permit you to carry out this task.";


            if(configuration.isJsonResponse()){

                ObjectNode output = Json.newObject();

                output.put("message",message);

                return F.Promise.<Result>pure(ok(output));
            }

            flash("warning", message);

            return F.Promise.pure(redirect(routes.Dashboard.index()));


        }*/

        play.Logger.info("User has the required role");

        return delegate.call(context);
    }
}

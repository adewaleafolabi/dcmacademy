package controllers;

import models.Forms.ContactForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import utils.Mail;

/**
 * Created by wale on 3/8/15.
 */
public class ContactController extends Controller {

    public Result index(){
        Form<ContactForm> contactForm = Form.form(ContactForm.class);

        return ok(views.html.contact.render(contactForm));
    }


    public Result save(){

        Form<ContactForm> contactForm = Form.form(ContactForm.class).bindFromRequest();



        if(contactForm.hasErrors()){

            return badRequest(views.html.contact.render(contactForm));
        }

        ContactForm contactFormData = contactForm.get();

        flash("success","Thank you. We would get back to you shortly");


        String message = "Dear "+contactFormData.fullName+", <br> Thank you for reaching out to us. This is to acknowledge your message. <p>kindly note that someone from our team would"+
                " reach out to you soon. <br/> Regards";

        Mail.Envelop envelop = new Mail.Envelop("Message Received", message, contactFormData.email);

        Mail.sendMail(envelop);

        return redirect(routes.ContactController.index());
    }
}

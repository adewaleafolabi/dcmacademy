package controllers;

import models.Repo;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.File;

/**
 * Created by wale on 5/11/15.
 */
public class DocumentLoaderController extends Controller {

    public Result file(String fileName){

        return fileLoader(Repo.DOCUMENTS,fileName);
    }

    private Result fileLoader(Repo repo, String filename){

        File document = new File(repo.getRepoPath(), filename);

        if (document.exists()) {

            return ok(document);

        }else{

            return notFound("The requested document was not found");
        }

    }
}

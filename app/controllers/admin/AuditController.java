package controllers.admin;

import SharedFunctions.ControllerShared;
import controllers.RoleSecured;
import controllers.Secured;
import models.AuditTrails.Login;
import models.Role;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class AuditController extends ControllerShared {


    public Result index(){

        List<Login> logins = Login.find.all();

        return ok(views.html.audit.list.render(logins));

    }

}

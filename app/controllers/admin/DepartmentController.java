package controllers.admin;

import controllers.RoleSecured;
import controllers.Secured;
import models.Department;
import models.Faculty;
import models.Role;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class DepartmentController extends Controller {


    public static Promise<Result> index() {
        return Department.find().map(new F.Function<List<Department>, Result>() {
            @Override
            public Result apply(List<Department> departments) throws Throwable {
                return ok(views.html.department.list.render(departments));
            }
        });
    }

    public static  Promise<Result>  create() {

        return Faculty.find().map(new F.Function<List<Faculty>, Result>() {
            @Override
            public Result apply(List<Faculty> faculties) throws Throwable {

                Form<Department> departmentForm = Form.form(Department.class);

                return ok(views.html.department.create.render(departmentForm,faculties));
            }

        });


    }


    public static  Promise<Result>  edit(final long id) {

        return Department.find(id).map(new F.Function<Department, Result>() {

            @Override
            public Result apply(Department department) throws Throwable {

                Form<Department> departmentForm = Form.form(Department.class).fill(department);

                return ok(views.html.department.edit.render(departmentForm,Faculty.find.all()));
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {

                play.Logger.error("Department lookup with id[{}] returned null",id);

                play.Logger.error("Error encountered while trying to return Department with id {}",id);

                play.Logger.error("Error details -- {}",throwable);

                flash("warning", "The specified department has no records");

                return redirect(controllers.admin.routes.DepartmentController.index());
            }
        });
    }

    public static Promise<Result> save() {

        final Form<Department> departmentForm = Form.form(Department.class).bindFromRequest();

        if(departmentForm.hasErrors()){

            play.Logger.error("Form binding failed with errors -- {}",departmentForm.errorsAsJson().toString());

            return Promise.<Result>pure(badRequest(views.html.department.create.render(departmentForm,Faculty.find.all())));
        }

        Department department = departmentForm.get();

        return department.asyncSave().map(new F.Function<Void, Result>() {
            @Override
            public Result apply(Void aVoid) throws Throwable {

                play.Logger.error("Saving department details");

                flash("success", "Department information has been saved");

                return redirect(controllers.admin.routes.DepartmentController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {


                String flashMessage = "";
                play.Logger.error("Problem in saving department information {}",throwable);

                if(throwable.getCause() instanceof SQLIntegrityConstraintViolationException){

                    flashMessage = "The supplied code or name is already in use";
                }else{

                    flashMessage = "A technical error occurred. Please try again later";
                }

                flash("danger", flashMessage);


                return badRequest(views.html.department.create.render(departmentForm,Faculty.find.all()));
            }
        });


    }

    public static Promise<Result> update() {
        final Form<Department> departmentForm = Form.form(Department.class).bindFromRequest();

        if(departmentForm.hasErrors()){

            play.Logger.error("Form binding failed with errors -- {}",departmentForm.errorsAsJson().toString());

            return Promise.<Result>pure(badRequest(views.html.department.edit.render(departmentForm,Faculty.find.all())));
        }

        Department department = departmentForm.get();

        return department.asyncMerge().map(new F.Function<Void, Result>() {
            @Override
            public Result apply(Void aVoid) throws Throwable {

                play.Logger.error("Updating department details");

                flash("success", "Department information has been updated");

                return redirect(controllers.admin.routes.DepartmentController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {


                String flashMessage = "";
                play.Logger.error("Problem in updating department information {}",throwable);

                if(throwable.getCause() instanceof SQLIntegrityConstraintViolationException){

                    flashMessage = "The supplied code or name is already in use";
                }else{

                    flashMessage = "A technical error occurred. Please try again later";
                }

                flash("danger", flashMessage);


                return badRequest(views.html.department.edit.render(departmentForm,Faculty.find.all()));
            }
        });
    }

    public static Result enable(long id) {
        return TODO;
    }

    public static Result disable(long id) {
        return TODO;
    }
}

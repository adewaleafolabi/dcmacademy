package controllers.admin;

import SharedFunctions.ControllerShared;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Update;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.RoleSecured;
import controllers.Secured;
import models.Course;
import models.FeaturedCourse;
import models.Forms.FeaturedCourseForm;
import models.Lecturer;
import models.Role;
import play.data.Form;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Security;

import java.util.Collections;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class FeaturedCourseController extends ControllerShared {

    public Result index(){



        return ok(views.html.featuredcourse.list.render(FeaturedCourse.find.all()));

    }

    public Result add(){

        Form<FeaturedCourseForm>  featuredCourseForm = Form.form(FeaturedCourseForm.class);


        return ok(views.html.featuredcourse.create.render(featuredCourseForm,Course.find.all()));

    }


    public Result save(){

        Form<FeaturedCourseForm>  featuredCourseForm = Form.form(FeaturedCourseForm.class).bindFromRequest();


        FeaturedCourseForm featuredCourseForm1 = featuredCourseForm.get();

        play.Logger.error("F {}",Json.toJson(featuredCourseForm1));


        if(featuredCourseForm1.courses!=null) {
            play.Logger.debug("Found courses");
            featuredCourseForm1.courses.removeAll(Collections.singleton(null));


            for (Long i : featuredCourseForm1.courses) {

                Course course = Course.find.byId(i);

                if (course != null) {



                    FeaturedCourse featuredCourse = new FeaturedCourse();
                    featuredCourse.course = course;
                    featuredCourse.save();
                }
            }
        }
        flash("success", "Complete");

        return redirect(routes.FeaturedCourseController.index());

    }

    public Result remove(long id){

        Update<FeaturedCourse> upd = Ebean.createUpdate(FeaturedCourse.class, "DELETE from FeaturedCourse WHERE id=:id");
        upd.set("id", ""+id);


        ObjectNode output = Json.newObject();



        try{
            upd.execute();

            output.put("message", "Course has been removed");

        }catch (Exception e){
            output.put("message", "Course was not removed. Technical error");

            play.Logger.error("{}",e);
        }

        return ok(output);


    }


}

package controllers.admin;

import SharedFunctions.ControllerShared;
import com.avaje.ebean.Page;
import com.avaje.ebean.PagingList;
import controllers.RoleSecured;
import controllers.Secured;
import models.*;
import models.Forms.CourseCreationForm;
import org.apache.commons.io.FileUtils;
import play.Play;
import play.cache.Cache;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

import java.io.File;
import java.sql.SQLIntegrityConstraintViolationException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 2/16/15.
 */
@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class CourseController extends ControllerShared{

    public static Promise<Result> index() {

        return Course.find().map(new F.Function<List<Course>, Result>() {
            @Override
            public Result apply(List<Course> courses) throws Throwable {
                return ok(views.html.course.list.render(courses));
            }
        });
    }



    public static  Promise<Result>  create() {

        return Department.find().map(new F.Function<List<Department>, Result>() {
            @Override
            public Result apply(List<Department> departments) throws Throwable {

                Form<CourseCreationForm> courseForm = Form.form(CourseCreationForm.class);

                return ok(views.html.course.create.render(courseForm,departments, Lecturer.find.all()));
            }

        });


    }


    public static Promise<Result> save() {

        final String IMG_REPO = Play.application().configuration().getString("app.image.storage");

        final String IMG_REPO_COURSE = IMG_REPO +"/COURSE";

        final Form<CourseCreationForm> courseForm = Form.form(CourseCreationForm.class).bindFromRequest();

        final Promise<Result> createFormTemplate = Promise.<Result>pure(badRequest(views.html.course.create.render(courseForm,Department.find.all(),Lecturer.find.all())));

        if(courseForm.hasErrors()){

            play.Logger.error("Form binding failed with errors -- {}",courseForm.errorsAsJson().toString());

            return createFormTemplate;
        }



        boolean imageAvailable = false;

        Http.MultipartFormData body = request().body().asMultipartFormData();

        if (body == null) {

            play.Logger.error("Request does not come with enctype=multipart/form-data.");

            flash("danger","Form contains invalid data");

            return Promise.<Result>pure(redirect(controllers.admin.routes.CourseController.create()));
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("file");


        if (filePart == null) {

            play.Logger.info("Form does not contain an image");

        }else{


            List <String> allowed = new ArrayList<>();
            allowed.add("image/jpeg");
            //allowed.add("image/png");



              //  if (!"image/jpeg".equalsIgnoreCase(filePart.getContentType()) || !"image/png".equalsIgnoreCase(filePart.getContentType())) {

            if(!allowed.contains(filePart.getContentType().toLowerCase())){
                    play.Logger.error("Supplied image is not a jpeg or png. {}",filePart.getContentType());
                    flash("warning", "Only jpegs are allowed as cover photos");
                    return createFormTemplate;
                }

            try {
                String extension = filePart.getContentType().toLowerCase().equals("image/jpeg") ? ".jpg" : ".png";
                File file = filePart.getFile();


                String destinationFileName = courseForm.get().title.replace(' ', '_') + extension;

                File destination = new File(IMG_REPO_COURSE, destinationFileName);

                if (destination.exists()) {
                    destination.renameTo(new File(IMG_REPO_COURSE, destinationFileName + "-" + new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date())));
                }

                FileUtils.moveFile(file, destination);
            }
            catch (Exception e){
               play.Logger.error("Error occured in saving cover photo",e);
               flash("warning", "Cover Photo was not saved");
            }

        }


        CourseCreationForm courseCreation = courseForm.get();


        Course course = new Course();
        course.department = Department.find.byId(courseCreation.department.id);
        course.objective = courseCreation.objective;
        course.outline = courseCreation.outline;
        course.title = courseCreation.title;
        course.duration = courseCreation.duration;


        if(courseCreation.lecturers!=null) {
            courseCreation.lecturers.removeAll(Collections.singleton(null));


            for (Long i : courseCreation.lecturers) {

                Lecturer lecturer = Lecturer.find.byId(i);

                if (lecturer != null) {

                    play.Logger.info("Lecturer found with id {}", lecturer.id);

                    course.lecturers.add(lecturer);
                }
            }
        }








        return course.asyncSave().map(new F.Function<Void, Result>() {
            @Override
            public Result apply(Void aVoid) throws Throwable {

                //play.Logger.debug("Saving {}",course.toString());
                flash("success", "Course information has been saved");

                return redirect(controllers.admin.routes.CourseController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {


                String flashMessage = "";
                play.Logger.error("Problem in saving department information {}",throwable);

                if(throwable.getCause() instanceof SQLIntegrityConstraintViolationException){

                    flashMessage = "The supplied title is already in use";
                }else{

                    flashMessage = "A technical error occurred. Please try again later";
                }

                flash("danger", flashMessage);




                return badRequest(views.html.course.create.render(courseForm,Department.find.all(),Lecturer.find.all()));
            }
        });


    }

    public static Promise<Result> edit(long id) {

        return Course.find(id).map(new F.Function<Course, Result>() {
            @Override
            public Result apply(Course course) throws Throwable {
                if(course == null){

                    flash("danger","The specified course was not found");

                    return redirect(controllers.admin.routes.CourseController.index());
                }



                List<Long> selectedLecturers = new ArrayList<Long>();

                for(Lecturer l : course.lecturers){
                    selectedLecturers.add(l.id);
                }

                CourseCreationForm courseCreationFormData = new CourseCreationForm(course.id,course.title,course.outline,course.objective,course.duration,course.department,selectedLecturers);

                Form<CourseCreationForm> courseCreationForm = Form.form(CourseCreationForm.class);



                return ok(views.html.course.edit.render(courseCreationForm.fill(courseCreationFormData),Department.find.all(),Lecturer.find.all()));
            }
        });

    }



    public static Promise<Result> update() {

        final String IMG_REPO = Play.application().configuration().getString("app.image.storage");

        final String IMG_REPO_COURSE = IMG_REPO +"/COURSE";

        final Form<CourseCreationForm> courseForm = Form.form(CourseCreationForm.class).bindFromRequest();

        final Promise<Result> createFormTemplate = Promise.<Result>pure(badRequest(views.html.course.edit.render(courseForm,Department.find.all(),Lecturer.find.all())));

        if(courseForm.hasErrors()){

            play.Logger.error("Form binding failed with errors -- {}",courseForm.errorsAsJson().toString());

            return createFormTemplate;
        }



        boolean imageAvailable = false;

        Http.MultipartFormData body = request().body().asMultipartFormData();

        if (body == null) {

            play.Logger.error("Request does not come with enctype=multipart/form-data.");

            flash("danger","Form contains invalid data");

            return createFormTemplate;
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("file");


        if (filePart == null) {

            play.Logger.info("Form does not contain an image");

        }else{

            List <String> allowed = new ArrayList<>();
            allowed.add("image/jpeg");

            if(!allowed.contains(filePart.getContentType().toLowerCase())){
                play.Logger.error("Supplied image is not a jpeg or png. {}",filePart.getContentType());
                flash("warning", "Only jpegs are allowed as cover photos");
                return createFormTemplate;
            }

            try {
                String extension = filePart.getContentType().toLowerCase().equals("image/jpeg") ? ".jpg" : ".png";
                File file = filePart.getFile();


                String destinationFileName = courseForm.get().title.replace(' ', '_') + extension;

                File destination = new File(IMG_REPO_COURSE, destinationFileName);

                if (destination.exists()) {
                    destination.renameTo(new File(IMG_REPO_COURSE, destinationFileName + "-" + new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date())));
                }

                FileUtils.moveFile(file, destination);
            }
            catch (Exception e){
                play.Logger.error("Error occured in saving cover photo",e);
                flash("warning", "Cover Photo was not saved");
            }

        }


        CourseCreationForm courseCreation = courseForm.get();


        Course course = Course.find.byId(courseCreation.id);

        if(course == null){
            flash("danger","Course was not found");

            return  Promise.<Result>pure(redirect(controllers.admin.routes.CourseController.index()));
        }
        course.department = Department.find.byId(courseCreation.department.id);
        course.objective = courseCreation.objective;
        course.outline = courseCreation.outline;
        course.title = courseCreation.title;
        course.duration = courseCreation.duration;


        if(courseCreation.lecturers!=null) {
            courseCreation.lecturers.removeAll(Collections.singleton(null));

            course.lecturers.clear();

            for (Long i : courseCreation.lecturers) {

                play.Logger.debug("id {}",i);

                Lecturer lecturer = Lecturer.find.byId(i);

                if (lecturer != null) {

                    play.Logger.info("Lecturer found with id {}", lecturer.id);

                    course.lecturers.add(lecturer);
                }
            }
        }








        return course.asyncUpdate().map(new F.Function<Void, Result>() {
            @Override
            public Result apply(Void aVoid) throws Throwable {

                flash("success", "Course information has been updated");

                return redirect(controllers.admin.routes.CourseController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {


                String flashMessage = "";
                play.Logger.error("Problem in updating course information {}",throwable);


                flash("danger", "A technical error occurred. Please try again later");

                return badRequest(views.html.course.edit.render(courseForm,Department.find.all(),Lecturer.find.all()));
            }
        });


    }

    public static Result enable(long id) {
        return TODO;
    }

    public static Result disable(long id) {
        return TODO;
    }
}

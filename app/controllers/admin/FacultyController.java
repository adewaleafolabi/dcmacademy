package controllers.admin;

import controllers.RoleSecured;
import controllers.Secured;
import models.Faculty;
import models.Lecturer;
import models.Role;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class FacultyController extends Controller {


    public static Promise<Result> index() {
        return Faculty.find().map(new F.Function<List<Faculty>, Result>() {
            @Override
            public Result apply(List<Faculty> faculties) throws Throwable {
                return ok(views.html.faculty.list.render(faculties));
            }
        });
    }

    public static  Result  create() {

        Form<Faculty> facultyForm = Form.form(Faculty.class);

        return ok(views.html.faculty.create.render(facultyForm, Lecturer.find.all()));

    }


    public static Result view(long id) {
        return TODO;
    }



    public static  Promise<Result>  edit(final long id) {

        return Faculty.find(id).map(new F.Function<Faculty, Result>() {

            @Override
            public Result apply(Faculty faculty) throws Throwable {

                Form<Faculty> facultyForm = Form.form(Faculty.class).fill(faculty);

                return ok(views.html.faculty.edit.render(facultyForm, Lecturer.find.all()));
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {

                play.Logger.error("Faculty lookup with id[{}] returned null",id);

                play.Logger.error("Error encountered while trying to return Faculty with id {}",id);

                play.Logger.error("Error details -- {}",throwable);

                flash("warning", "The specified faculty has no records");

                return redirect(controllers.admin.routes.FacultyController.index());
            }
        });
    }

    public static Promise<Result> save() {

        final Form<Faculty> facultyForm = Form.form(Faculty.class).bindFromRequest();

        if(facultyForm.hasErrors()){

            play.Logger.error("Form binding failed with errors -- {}",facultyForm.errorsAsJson().toString());

            return Promise.<Result>pure(badRequest(views.html.faculty.create.render(facultyForm, Lecturer.find.all())));
        }

        Faculty faculty = facultyForm.get();
        faculty.dean = Lecturer.find.byId(faculty.dean.id);

        return faculty.asyncSave().map(new F.Function<Void, Result>() {
            @Override
            public Result apply(Void aVoid) throws Throwable {

                play.Logger.error("Saving faculty details");

                flash("success", "Faculty information has been saved");

                return redirect(controllers.admin.routes.FacultyController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {


                String flashMessage = "";
                play.Logger.error("Problem in saving faculty information {}",throwable);

                if(throwable.getCause() instanceof SQLIntegrityConstraintViolationException){

                    flashMessage = "The supplied code or name is already in use";
                }else{

                    flashMessage = "A technical error occurred. Please try again later";
                }

                flash("danger", flashMessage);


                return badRequest(views.html.faculty.create.render(facultyForm, Lecturer.find.all()));
            }
        });


    }

    public static Promise<Result> update() {
        final Form<Faculty> facultyForm = Form.form(Faculty.class).bindFromRequest();

        if(facultyForm.hasErrors()){

            play.Logger.error("Form binding failed with errors -- {}",facultyForm.errorsAsJson().toString());

            return Promise.<Result>pure(badRequest(views.html.faculty.edit.render(facultyForm, Lecturer.find.all())));
        }

        Faculty faculty = facultyForm.get();
        faculty.dean = Lecturer.find.byId(faculty.dean.id);

        return faculty.asyncMerge().map(new F.Function<Void, Result>() {
            @Override
            public Result apply(Void aVoid) throws Throwable {

                play.Logger.error("Updating faculty details");

                flash("success", "Faculty information has been updated");

                return redirect(controllers.admin.routes.FacultyController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {


                String flashMessage = "";
                play.Logger.error("Problem in updating faculty information {}",throwable);

                if(throwable.getCause() instanceof SQLIntegrityConstraintViolationException){

                    flashMessage = "The supplied code or name is already in use";
                }else{

                    flashMessage = "A technical error occurred. Please try again later";
                }

                flash("danger", flashMessage);


                return badRequest(views.html.faculty.edit.render(facultyForm, Lecturer.find.all()));
            }
        });
    }

    public static Result enable(long id) {
        return TODO;
    }

    public static Result disable(long id) {
        return TODO;
    }
}

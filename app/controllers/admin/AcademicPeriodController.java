package controllers.admin;

import SharedFunctions.ControllerShared;
import controllers.RoleSecured;
import controllers.Secured;
import models.AcademicPeriod;
import models.Course;
import models.Forms.AcademicPeriodForm;
import models.Lecturer;
import models.Role;
import play.data.Form;
import play.libs.F;
import play.mvc.Result;
import play.mvc.Security;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class AcademicPeriodController extends ControllerShared {

    public F.Promise<Result> index(){

        return AcademicPeriod.find().map(new F.Function<List<AcademicPeriod>, Result>() {
            @Override
            public Result apply(List<AcademicPeriod> academicPeriods) throws Throwable {

                return ok(views.html.academicperiod.list.render(academicPeriods));
            }
        });


    }


    public F.Promise<Result> create(){

        return Course.find().map(new F.Function<List<Course>, Result>() {
            @Override
            public Result apply(List<Course> courses) throws Throwable {

                Form<AcademicPeriodForm> academicPeriodForm = Form.form(AcademicPeriodForm.class);

                return ok(views.html.academicperiod.create.render(academicPeriodForm,courses));
            }
        });


    }


    public Result save(){

        Form<AcademicPeriodForm> academicPeriodForm = Form.form(AcademicPeriodForm.class).bindFromRequest();

        List<Course> courses = Course.find.all();


        if(academicPeriodForm.hasErrors()){

            return badRequest(views.html.academicperiod.create.render(academicPeriodForm,courses));
        }

        AcademicPeriodForm academicPeriodData = academicPeriodForm.get();

        play.Logger.info("Data Received {}",academicPeriodData.toString());


        AcademicPeriod academicPeriod = new AcademicPeriod();
        academicPeriod.name = academicPeriodData.name+" - "+(new SimpleDateFormat("yyyy-MM-dd").format(academicPeriodData.startDate))+" - "+(new SimpleDateFormat("yyyy-MM-dd").format(academicPeriodData.endDate));
        academicPeriod.startDate = academicPeriodData.startDate;
        academicPeriod.endDate = academicPeriodData.endDate;
        academicPeriod.gracePeriod = academicPeriodData.gracePeriod;
        academicPeriod.enrollmentDuration = academicPeriodData.enrollmentDuration;


        if(academicPeriodData.availableCourses!=null) {
            academicPeriodData.availableCourses.removeAll(Collections.singleton(null));

            for(int i =0; i< academicPeriodData.availableCourses.size();i++){

                long id = academicPeriodData.availableCourses.get(i);

                Course course = Course.find.byId(id);

                play.Logger.info("Looking up course with id {}",id);

                if(course != null){

                    play.Logger.info("Found course[id={}]",id);

                    if(academicPeriod.availableCourses == null){

                        academicPeriod.availableCourses = new ArrayList<>();
                    }

                    academicPeriod.availableCourses.add(course);
                }
            }

        }

        try{

            academicPeriod.save();

        }catch (Exception e){

            play.Logger.error("Academic period save failed{}",e);

            flash("danger","Technical error. Please try again");

            return badRequest(views.html.academicperiod.create.render(academicPeriodForm,courses));

        }

        flash("success","Academic Period Created");

        return redirect(routes.AcademicPeriodController.index());

    }

}

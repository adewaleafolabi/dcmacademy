package controllers.admin;

import SharedFunctions.ControllerShared;
import controllers.RoleSecured;
import controllers.Secured;
import models.*;
import play.data.Form;
import play.libs.F;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

import java.util.List;

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class PaymentController extends ControllerShared {


    public F.Promise<Result> index() {
        return FeePayment.find().map(new F.Function<List<FeePayment>, Result>() {
            @Override
            public Result apply(List<FeePayment> feePayments) throws Throwable {
                return ok(views.html.payment.list.render(feePayments));
            }
        });
    }




    public  Result create() {

        Form<FeePayment> paymentRecord  = Form.form(FeePayment.class);

        return ok(views.html.payment.create.render(paymentRecord, Student.find.all(), Course.find.all() , AcademicPeriod.find.all()));
    }


    public  Result save() {


        Form<FeePayment> paymentRecord  = Form.form(FeePayment.class).bindFromRequest();

        Result BAD_FORM = badRequest(views.html.payment.create.render(paymentRecord, Student.find.all(), Course.find.all() ,  AcademicPeriod.find.all()));

        if(paymentRecord.hasErrors() ){

            play.Logger.error("Fom has errors {}",paymentRecord.errorsAsJson().toString());

            return BAD_FORM;
        }

        FeePayment paymentRecordData = paymentRecord.get();



        CourseRegistration courseRegistration = CourseRegistration.find.where()
                .eq("student.id",paymentRecordData.student.id)
                .eq("course.id",paymentRecordData.course.id)
                .eq("academicPeriod.id",paymentRecordData.academicPeriod.id)
                .findUnique();

        if(courseRegistration == null){

            flash("warning","Student is not registered for this course in the academic period");


            return badRequest(views.html.payment.create.render(paymentRecord, Student.find.all(), Course.find.all() , AcademicPeriod.find.all()));
        }




        FeePayment entry = new FeePayment();
        entry.amount  = paymentRecordData.amount;
        entry.course = courseRegistration.course;
        entry.student = courseRegistration.student;
        entry.paymentDate = paymentRecordData.paymentDate;
        entry.paymentMethod = paymentRecordData.paymentMethod;
        entry.paymentReference = paymentRecordData.paymentReference;
        entry.academicPeriod = courseRegistration.academicPeriod;

        courseRegistration.student.payments.add(entry);


        try{
            courseRegistration.student.update();

        }catch (Exception e){

            play.Logger.error("{}",e);

            flash("danger","Technical error. Please try again");

            return badRequest(views.html.payment.create.render(paymentRecord, Student.find.all(), Course.find.all(), AcademicPeriod.find.all() ));
        }

        flash("success","Record has been saved");

        return redirect(routes.PaymentController.index());
    }

    public  Result edit(long id) {
        return TODO;
    }



    public  Result update() {
        return TODO;
    }


}
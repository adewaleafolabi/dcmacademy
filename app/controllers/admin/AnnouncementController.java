package controllers.admin;

import SharedFunctions.ControllerShared;
import controllers.RoleSecured;
import controllers.Secured;
import models.Course;
import models.GeneralAnnouncement;
import models.GeneralAnnouncement;
import models.Role;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by wale on 5/6/15.
 */

@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class AnnouncementController extends ControllerShared {


    public Result index(){


        return ok(views.html.announcement.list.render(GeneralAnnouncement.find.all()));


    }


    public Result create(){

        Form<GeneralAnnouncement> announcementForm = Form.form(GeneralAnnouncement.class);

        return ok(views.html.announcement.create.render(announcementForm));

    }


    public Result save(){

        Form<GeneralAnnouncement> announcementForm = Form.form(GeneralAnnouncement.class).bindFromRequest();

        Result createTemplate = badRequest(views.html.announcement.create.render(announcementForm));

        if(announcementForm.hasErrors()){

            play.Logger.error("Form error{}",announcementForm.errorsAsJson().asText());

            return badRequest(views.html.announcement.create.render(announcementForm));
        }

        GeneralAnnouncement announcementFormData = announcementForm.get();

        GeneralAnnouncement announcement = new GeneralAnnouncement();
        announcement.message = announcementFormData.message;
        announcement.user = hm.getCurrentUser();


        try{

            play.Logger.debug("", announcement.toString());

            announcement.save();

            play.Logger.debug("GeneralAnnouncement saved");

        }catch (Exception exp){

            play.Logger.error("Saving announcement{}",exp);

            return createTemplate;
        }


        flash("success", "Course announcement has been saved");

        return redirect(routes.AnnouncementController.index());

    }
}

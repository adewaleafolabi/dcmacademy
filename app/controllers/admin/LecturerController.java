package controllers.admin;

import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.RoleSecured;
import controllers.Secured;
import models.*;
import models.Forms.LecturerForm;
import org.apache.commons.io.FileUtils;
import play.Play;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;
import utils.AppException;
import utils.Hash;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 2/16/15.
 */
@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.ADMIN)
public class LecturerController extends Controller {


    public static Promise<Result> index() {
        return Lecturer.find().map(new F.Function<List<Lecturer>, Result>() {
            @Override
            public Result apply(List<Lecturer> lecturers) throws Throwable {
                return ok(views.html.lecturer.list.render(lecturers));
            }
        });
    }

    public static Result create() {
        Form<LecturerForm> lecturerForm = Form.form(LecturerForm.class);
        return ok(views.html.lecturer.create.render(lecturerForm));
    }

    public static Result save() {

        final String IMG_REPO = Play.application().configuration().getString("app.image.storage");

        final String IMG_REPO_LECTURER = IMG_REPO +"/LECTURER";

        Form<LecturerForm> lecturerForm = Form.form(LecturerForm.class).bindFromRequest();

        if(lecturerForm.hasErrors()){
            play.Logger.error("LecturerCreate Form has errors {}", lecturerForm.errorsAsJson().toString());
            return badRequest(views.html.lecturer.create.render(lecturerForm));
        }

        LecturerForm lecturerData = lecturerForm.get();







        BioData bioData = new BioData();
        bioData.firstName=lecturerData.firstName;
        bioData.middleName=lecturerData.middleName;
        bioData.lastName=lecturerData.lastName;
        bioData.salutation = lecturerData.salutation;
        bioData.additionalTitles = lecturerData.additionalTitles;


        User user = new User();
        user.role= Role.LECTURER;
        user.username = lecturerData.email;
        user.bioData = bioData;
        bioData.user= user;


        Lecturer lecturer = new Lecturer();
        lecturer.profile= lecturerData.profile;
        lecturer.user =user;
        user.lecturer = lecturer;

        try{

            user.passwordHash = Hash.createPassword(lecturerData.password);
            user.save();
        }
        catch (AppException appEx){

            play.Logger.error("Error in creating hash -- {}",appEx);

            flash("warning","Lecturer was created but login password had some issues.");
        }
        catch (Exception exp){

            flash("danger","A technical error occurred. please try again");

            play.Logger.error("General error {}",exp);

            return badRequest(views.html.lecturer.create.render(lecturerForm));
        }


        flash("success","Lecturer created successfully");




        Http.MultipartFormData body = request().body().asMultipartFormData();

        if (body == null) {

            play.Logger.error("Request does not come with enctype=multipart/form-data.");

            return redirect(controllers.admin.routes.LecturerController.index());
        }

        Http.MultipartFormData.FilePart filePart = body.getFile("file");


        if (filePart == null) {

            play.Logger.info("Form does not contain an image");

            return redirect(controllers.admin.routes.LecturerController.index());

        }else{


            List <String> allowed = new ArrayList<>();
            allowed.add("image/jpeg");

            if(!allowed.contains(filePart.getContentType().toLowerCase())){
                play.Logger.error("Supplied image is not a jpeg or png. {}",filePart.getContentType());
                flash("warning", "Profile image was not set as only jpegs are allowed");

                return redirect(controllers.admin.routes.LecturerController.index());
            }

            try {
                String extension = filePart.getContentType().toLowerCase().equals("image/jpeg") ? ".jpg" : ".png";

                File file = filePart.getFile();


                String destinationFileName = lecturer.id+ extension;

                File destination = new File(IMG_REPO_LECTURER, destinationFileName);

                if (destination.exists()) {
                    destination.renameTo(new File(IMG_REPO_LECTURER, destinationFileName + "-" + new SimpleDateFormat("yyyyMMddHHmmssS").format(new Date())));
                }

                FileUtils.moveFile(file, destination);
            }
            catch (Exception e){
                play.Logger.error("Error occured in saving cover photo",e);
                flash("warning", "Cover Photo was not saved");
            }

        }
        return redirect(controllers.admin.routes.LecturerController.index());



    }

    public static Promise<Result> view(final long id) {

        return Lecturer.find(id).map(new F.Function<Lecturer, Result>() {
            @Override
            public Result apply(Lecturer lecturer) throws Throwable {
                return ok(views.html.lecturer.show.render(lecturer));
            }
        });


    }

    public static Promise<Result> edit(long id) {
        return Lecturer.find(id).map(new F.Function<Lecturer, Result>() {
            @Override
            public Result apply(Lecturer lecturer) throws Throwable {
                LecturerForm lecturerForm  = new LecturerForm(lecturer);
                return ok(views.html.lecturer.edit.render(Form.form(LecturerForm.class).fill(lecturerForm)));
            }
        });
    }



    public static Promise<Result> update() {
        final Form<LecturerForm> lecturerForm = Form.form(LecturerForm.class).bindFromRequest();

        if(lecturerForm.hasErrors()){
            play.Logger.error("LecturerCreate Form has errors {}", lecturerForm.errorsAsJson().toString());
            return Promise.<Result>pure(badRequest(views.html.lecturer.edit.render(lecturerForm)));
        }

        final LecturerForm lecturerData = lecturerForm.get();

        return Lecturer.find(lecturerData.id).map(new F.Function<Lecturer, Result>() {
            @Override
            public Result apply(Lecturer lecturer) throws Throwable {
                lecturer.profile = lecturerData.profile;
                lecturer.user.bioData.firstName = lecturerData.firstName;
                lecturer.user.bioData.lastName = lecturerData.lastName;
                lecturer.user.bioData.middleName = lecturerData.middleName;
                lecturer.user.username = lecturerData.email;
                lecturer.user.bioData.additionalTitles = lecturerData.additionalTitles;
                lecturer.user.bioData.salutation = lecturerData.salutation;
                lecturer.update();



                flash("success","Lecturer data updated");
                return redirect(controllers.admin.routes.LecturerController.index());
            }
        }).recover(new F.Function<Throwable, Result>() {
            @Override
            public Result apply(Throwable throwable) throws Throwable {
                play.Logger.error("Error occurred in updating lecturer -- {}", throwable);

                flash("warning","A technical errror occurred");

                return badRequest(views.html.lecturer.create.render(lecturerForm));
            }
        });



    }

    public static Promise<Result> enable(long id) {
        return Lecturer.find(id).map(new F.Function<Lecturer, Result>() {
            @Override
            public Result apply(Lecturer lecturer) throws Throwable {

                ObjectNode output = Json.newObject();

                if(lecturer == null){
                    output.put("message","Kindly provide a valid lecturer");
                    return ok(output);
                }

                lecturer.user.isEnabled = true;

                User user = lecturer.user;
                user.isEnabled = true;

                try{
                    user.update();

                    output.put("message","Lecturer is enabled");

                    return ok(output);
                }
                catch (Exception e){
                    play.Logger.error("Failed to enable Lecturer{}",e);

                    output.put("message", "Technical Error. Try again");

                    return ok(output);
                }
            }
        });
    }

    public static Promise<Result> disable(long id) {

        return Lecturer.find(id).map(new F.Function<Lecturer, Result>() {
            @Override
            public Result apply(Lecturer lecturer) throws Throwable {

                ObjectNode output = Json.newObject();

                if(lecturer == null){
                    output.put("message","Kindly provide a valid lecturer");
                    return ok(output);
                }

                lecturer.user.isEnabled = false;

                User user = lecturer.user;
                user.isEnabled = false;

                try{
                    user.update();

                    output.put("message","Lecturer is disabled");

                    return ok(output);
                }
                catch (Exception e){
                    play.Logger.error("Failed to disable Lecturer{}",e);

                    output.put("message", "Technical Error. Try again");

                    return ok(output);
                }
            }
        });
    }


}

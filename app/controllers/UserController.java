package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.BioData;
import models.Forms.UserForm;
import models.Forms.UserForm;
import models.Role;
import models.User;
import models.User;
import play.data.Form;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import utils.AppException;
import utils.Hash;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wale on 2/16/15.
 */
@Security.Authenticated(Secured.class)
@RoleSecured(role = Role.SUPER_ADMIN)
public class UserController extends Controller {

    public static Promise<Result> index() {

        Map<String,Object> map = new HashMap<>();
        map.put("role",Role.ADMIN);
        return User.find(map).map(new F.Function<List<User>, Result>() {
            @Override
            public Result apply(List<User> users) throws Throwable {
                return ok(views.html.user.list.render(users));
            }
        });
    }

    public static Result create() {
        Form<UserForm> userForm = Form.form(UserForm.class);


        return ok(views.html.user.create.render(userForm));
    }


    public static Result save() {

        Form<UserForm>  userForm = Form.form(UserForm.class).bindFromRequest();

        if(userForm.hasErrors()){

            return badRequest(views.html.user.create.render(userForm));
        }

        UserForm userFormData = userForm.get();


        User user = new User();

        BioData bioData = new BioData();
        bioData.firstName = userFormData.firstName;
        bioData.lastName = userFormData.lastName;
        bioData.middleName = userFormData.middleName;
        bioData.gender = userFormData.gender;
        bioData.phoneNumber = userFormData.phoneNumber;
        bioData.residentialAddress = userFormData.residentialAddress;
        bioData.salutation = userFormData.salutation;

        bioData.user= user;

        user.bioData = bioData;
        user.username = userFormData.email;
        user.isEnabled = true;
        user.isValidated = true;
        
        user.role= Role.ADMIN;

        try{
            user.passwordHash = Hash.createPassword(userFormData.password);

            user.save();
        }catch (Exception exp){

            play.Logger.error("Error in processing user creation {}",exp);

            flash("danger","Technical Error.Please try again");

            
            return badRequest(views.html.user.create.render(userForm));
        }


        flash("success","User has been created");

        return redirect(routes.UserController.index());
    }

    public static Result view(long id) {
        return TODO;
    }

    public static Result edit(long id) {
        return TODO;
    }



    public static Result update() {

        Form<UserForm> userForm = Form.form(UserForm.class).bindFromRequest();


        if(userForm.hasErrors()){
            return badRequest(views.html.user.edit.render(userForm));
        }

        UserForm userFormData = userForm.get();

        User user = User.find.byId(userFormData.id);

        if(user == null){

            flash("danger","The user was not found");

            redirect(routes.UserController.index());
        }




        user.bioData.firstName = userFormData.firstName;
        user.bioData.lastName = userFormData.lastName;
        user.bioData.middleName = userFormData.middleName;
        user.bioData.phoneNumber = userFormData.phoneNumber;
        user.bioData.salutation = userFormData.salutation;
        user.bioData.residentialAddress = userFormData.residentialAddress;
        user.bioData.gender = userFormData.gender;
        user.bioData.phoneNumber = userFormData.phoneNumber;


        try{

            user.update();

            flash("success","User updated successfully");

            return redirect(routes.UserController.index());

        }catch(Exception exp){

            play.Logger.error("User update failed",exp);

            flash("warning","Technical error occured. Please try again");

            return ok(views.html.user.edit.render(userForm));

        }
    }

    public static Promise<Result> enable(long id) {
        return User.find(id).map(new F.Function<User, Result>() {
            @Override
            public Result apply(User user) throws Throwable {

                ObjectNode output = Json.newObject();

                if(user == null){
                    output.put("message","Kindly provide a valid user ID");
                    return ok(output);
                }

                user.isEnabled = true;

                try{
                    user.update();

                    output.put("message","User is enabled");

                    return ok(output);
                }
                catch (Exception e){
                    play.Logger.error("Failed to enable user{}",e);

                    output.put("message", "Technical Error. Try again");

                    return ok(output);
                }
            }
        });
    }

    public static Promise<Result> disable(long id) {
        return User.find(id).map(new F.Function<User, Result>() {
            @Override
            public Result apply(User user) throws Throwable {

                ObjectNode output = Json.newObject();

                if(user == null){
                    output.put("message","Kindly provide a valid user ID");
                    return ok(output);
                }

                user.isEnabled = true;

                try{
                    user.update();

                    output.put("message","User is disabled");

                    return ok(output);
                }
                catch (Exception e){
                    play.Logger.error("Failed to disable user{}",e);

                    output.put("message", "Technical Error. Try again");

                    return ok(output);
                }
            }
        });
    }


}

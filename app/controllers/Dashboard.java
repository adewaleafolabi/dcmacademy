package controllers;

import SharedFunctions.ControllerShared;
import models.Notification;
import models.Role;
import models.Student;
import models.User;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Created by wale on 2/24/15.
 */
@Security.Authenticated(Secured.class)
public class Dashboard extends ControllerShared {

    public Result index(){

        play.Logger.debug("{}",currentUser.role.name());

        switch (currentUser.role){

            case SUPER_ADMIN:
                play.Logger.info("I AM SUPER ADMIN");
                return ok(views.html.dashboard.admin.render());

            case ADMIN:
                play.Logger.info("I AM ADMIN");
                return ok(views.html.dashboard.admin.render());
            case LECTURER:
                play.Logger.info("I AM LECTURER");
                return ok(views.html.dashboard.lecturer.render());
            default:
                play.Logger.info("I AM STUDENT");
                Student s = Student.find.where().eq("user.username",session("auth_user_name")).findUnique();

                return ok(views.html.dashboard.student.render(s));

        }




    }
}

package utils;

import play.db.ebean.Model;

import java.util.List;

/**
 * Created by wale on 10/20/14.
 */
public class DataTable {
    int  iTotalRecords;

    int  iTotalDisplayRecords;

    String  sEcho;

    String sColumns;

    List<?> aaData;

    public int getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(int iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public int getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public String getsEcho() {
        return sEcho;
    }

    public void setsEcho(String sEcho) {
        this.sEcho = sEcho;
    }

    public String getsColumns() {
        return sColumns;
    }

    public void setsColumns(String sColumns) {
        this.sColumns = sColumns;
    }

    public List<?> getAaData() {
        return aaData;
    }

    public void setAaData(List<? extends Model> aaData) {
        this.aaData = aaData;
    }
}

package models;

/**
 * Created by wale on 5/24/15.
 */
public enum Status {
    PENDING,
    APPROVED
}

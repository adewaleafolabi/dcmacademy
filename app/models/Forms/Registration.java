package models.Forms;

import models.User;
import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.validation.Constraint;

/**
 * Created by wale on 2/16/15.
 */
public class Registration {

    @Formats.NonEmpty

    @Constraints.Required
    public String firstName;


    public String middleName;


    @Formats.NonEmpty
    @Constraints.Required
    public String lastName;

    @Constraints.Required
    @Constraints.MinLength(8)
    public String password;

    @Constraints.Required
    @Constraints.Email
    public String email;

    public String validate() {

        User user =  User.findByUserName(email);

        if(user !=null){
            return "The email provided is already registered. Would you like to login instead ? ";
        }
        return null;
    }

}

package models.Forms;

import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by wale on 3/8/15.
 */
public class ContactForm {

    @Formats.NonEmpty
    @Constraints.Required
    public String fullName;

    @Formats.NonEmpty
    @Constraints.Required
    @Constraints.Email
    public String email;

    @Formats.NonEmpty
    @Constraints.Required
    public String message;
}

package models.Forms;

import models.Department;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 3/2/15.
 */
public class CourseCreationForm {

    public long id;

    public String title;

    public String outline;

    public String objective;

    public int duration;

    public Department department;

    public List<Long> lecturers = new ArrayList<>();

    public CourseCreationForm() {
    }

    public CourseCreationForm(String title, String outline, String objective, int duration, Department department, List<Long> lecturers) {
        this.title = title;
        this.outline = outline;
        this.objective = objective;
        this.duration = duration;
        this.department = department;
        this.lecturers = lecturers;
    }


    public CourseCreationForm(long id, String title, String outline, String objective, int duration, Department department, List<Long> lecturers) {
        this.id= id;
        this.title = title;
        this.outline = outline;
        this.objective = objective;
        this.duration = duration;
        this.department = department;
        this.lecturers = lecturers;
    }


}

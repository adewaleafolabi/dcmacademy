package models.Forms;

import java.util.Date;
import java.util.List;

/**
 * Created by wale on 5/24/15.
 */
public class AcademicPeriodForm {

    public long id;

    public String name;

    public Date startDate;

    public Date endDate;

    public int enrollmentDuration;

    public int gracePeriod;


    public List<Long> availableCourses;
}

package models.Forms;

import models.Lecturer;
import models.User;
import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by wale on 3/5/15.
 */
public class LecturerForm {

    public long id;

    public String salutation;

    public String additionalTitles;

    @Formats.NonEmpty
    @Constraints.Required
    public String firstName;


    public String middleName;


    @Formats.NonEmpty
    @Constraints.Required
    public String lastName;


    public String password;

    @Constraints.Required
    @Constraints.Email
    public String email;

    public String profile;

    public LecturerForm() {
    }

    public LecturerForm(Lecturer lecturer) {
        this.profile = lecturer.profile;
        this.email= lecturer.user.username;
        this.firstName = lecturer.user.bioData.firstName;
        this.lastName= lecturer.user.bioData.lastName;
        this.middleName = lecturer.user.bioData.middleName;
        this.id = lecturer.id;
    }


    public String validate() {

        User user =  User.findByUserName(email);

        Lecturer lecturer = Lecturer.find.byId(id);

        if(user !=null) {

            if (lecturer != null && lecturer.user.id != user.id) {

                return "The email provided is already registered. Would you like to login instead ? ";


            }
        }
        return null;
    }
}

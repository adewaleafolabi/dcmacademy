package models.Forms;

import models.Gender;
import models.Student;
import models.User;
import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by wale on 3/5/15.
 */
public class UserForm {

    public long id;

    public String salutation;

    public String additionalTitles;

    @Formats.NonEmpty
    @Constraints.Required
    public String firstName;

    public String middleName;


    @Formats.NonEmpty
    @Constraints.Required
    public String lastName;


    public String password;

    @Constraints.Required
    @Constraints.Email
    public String email;


    public Gender gender;

    public String phoneNumber;

    public String residentialAddress;




    public String validate() {

        User user =  User.findByUserName(email);

        if(user !=null && id != user.id) {

                return "The email provided is already registered.";


            }

        return null;
    }
}

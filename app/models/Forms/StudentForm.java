package models.Forms;

import models.Gender;
import models.Lecturer;
import models.Student;
import models.User;
import play.data.format.Formats;
import play.data.validation.Constraints;

/**
 * Created by wale on 3/5/15.
 */
public class StudentForm {

    public long id;

    public String salutation;

    public String additionalTitles;

    @Formats.NonEmpty
    @Constraints.Required
    public String firstName;

    public String middleName;


    @Formats.NonEmpty
    @Constraints.Required
    public String lastName;


    public String password;

    @Constraints.Required
    @Constraints.Email
    public String email;

    public String profile;

    public Gender gender;

    public String phoneNumber;

    public String residentialAddress;

    public String studentNumber;

    public StudentForm() {
    }

    public StudentForm(Student student) {
        this.email= student.user.username;
        this.firstName = student.user.bioData.firstName;
        this.lastName= student.user.bioData.lastName;
        this.middleName = student.user.bioData.middleName;
        this.gender = student.user.bioData.gender;
        this.phoneNumber = student.user.bioData.phoneNumber;
        this.residentialAddress = student.user.bioData.residentialAddress;
        this.id = student.id;
        this.studentNumber = student.studentNumber;
    }


    public String validate() {

        User user =  User.findByUserName(email);

        Student student = Student.find.byId(id);
        if(user !=null) {

            if (student != null && student.user.id != user.id) {

                return "The email provided is already registered.";


            }
        }
        return null;
    }
}

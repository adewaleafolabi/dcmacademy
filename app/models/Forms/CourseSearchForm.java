package models.Forms;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 5/3/15.
 */
public class CourseSearchForm {

    public String title="";

    public int page =1;

    public List<Long> departments  = new ArrayList<>();

    private String urlParams ="?page="+page+"&";

    public void setPage(int page) {
        this.page = page;
    }

    public String generateUrlParams(){
        if(!title.equals("")){
            urlParams+="title="+ org.h2.util.StringUtils.urlEncode(title)+"&";
        }

        for(int i =0;i<departments.size();i++){
            urlParams+="departments[]="+departments.get(i)+"&";
        }

        return StringUtils.stripEnd(urlParams,"&");

    }
}

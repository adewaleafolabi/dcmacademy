package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.Logger;
import play.db.ebean.Model;
import play.libs.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;
import play.Logger;
import play.data.format.Formats;
import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;
import utils.AppException;
import utils.Hash;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 2/15/15.
 */
@Entity
public class Student extends Model {

    @Id
    public long id;

    @Enumerated(EnumType.STRING)
    private Role role = Role.STUDENT;

    public String studentNumber;

    @OneToOne(optional = false,cascade = CascadeType.ALL,fetch = FetchType.LAZY)//(optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",nullable = false)
    @JsonIgnore
    public User user;

    @ManyToMany(cascade = CascadeType.ALL,mappedBy = "students")
    public List<Course> courses;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "student")
    public List<ExamRecord> examRecords;


    @OneToMany(cascade = CascadeType.ALL,mappedBy = "student")
    public List<FeePayment> payments;

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "student")
    public List<CourseRegistration> courseRegistrations;


    @JoinTable(name = "course_recently_viewed", joinColumns = {
            @JoinColumn(name = "student_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "course_id", referencedColumnName = "id")})
    @ManyToMany
    public List<Course> recentlyViewedCourses;

    public static Finder<Long, Student> find = new Finder<Long, Student>(Long.class, Student.class);


    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public static F.Promise<List<Student>> find(){
        return F.Promise.promise(new F.Function0<List<Student>>() {
            @Override
            public List<Student> apply() throws Throwable {
                return find.all();
            }
        },ctx).recover(new F.Function<Throwable, List<Student>>() {
            @Override
            public List<Student> apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch Student. DB probably unreachable");

                return new ArrayList<Student>();
            }
        },ctx);
    }

    public static F.Promise<Student> find(final long id){
        return F.Promise.promise(new F.Function0<Student>() {
            @Override
            public Student apply() throws Throwable {

                return find.byId(id);

            }
        },ctx).recover(new F.Function<Throwable, Student>() {
            @Override
            public Student apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch student. DB probably unreachable {}",throwable);
                return new Student();
            }
        },ctx);
    }





    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, ctx);

    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student{");
        sb.append("id=").append(id);
        sb.append(", role=").append(role);
        sb.append(", studentNumber='").append(studentNumber).append('\'');
        sb.append(", user=").append("user{id="+user.id+"}");
        sb.append(", courses=").append(courses);
        sb.append(", recentlyViewedCourses=").append(recentlyViewedCourses);
        sb.append('}');
        return sb.toString();
    }
}

package models;

import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.List;

/**
 * Created by wale on 2/24/15.
 */
@Entity
public class Department extends Model{

    @Id
    public long id;

    public String name;
    
    @Column(unique = true)
    public String code;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public List<Course> courses;


    @OneToMany
    public List<Lecturer> lecturers;


    @ManyToOne
    public Faculty faculty;


    public static Model.Finder<Long, Department> find = new Model.Finder<Long, Department>(Long.class, Department.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<Department>> find(){

        return F.Promise.promise(new F.Function0<List<Department>>() {
            @Override
            public List<Department> apply() throws Throwable {
                return find.all();
            }
        });
    }

    public  static F.Promise<Department> find(final long id){

        return F.Promise.promise(new F.Function0<Department>() {
            @Override
            public Department apply() throws Throwable {
                return find.byId(id);
            }
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, ctx);

    }

    public F.Promise<Void> asyncMerge(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                if(id !=0){
                    update();
                }else {
                    save();
                }
                return null;
            }
        }, ctx);

    }


    
}

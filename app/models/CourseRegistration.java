package models;

import play.db.ebean.Model;
import play.libs.F;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import play.libs.Akka;



/**
 * Created by wale on 5/24/15.
 */
@Entity
public class CourseRegistration extends Model{

    @Id
    public long id;

    @ManyToOne
    public Student student;

    @ManyToOne
    public Course course;

    @OneToOne
    public AcademicPeriod academicPeriod;

    public Date registrationDate;

    public Status status;

    public static Finder<Long, CourseRegistration> find = new Finder<Long, CourseRegistration>(Long.class, CourseRegistration.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<CourseRegistration>> find(){

        return F.Promise.promise(new F.Function0<List<CourseRegistration>>() {
            @Override
            public List<CourseRegistration> apply() throws Throwable {
                return find.all();
            }
        }).recover(new F.Function<Throwable, List<CourseRegistration>>() {
            @Override
            public List<CourseRegistration> apply(Throwable throwable) throws Throwable {
                play.Logger.error("Problem in fetching course registration",throwable);
                return new ArrayList<CourseRegistration>();
            }
        });
    }


    public  static F.Promise<List<CourseRegistration>> findByStudentID(final long studentID){

        return F.Promise.promise(new F.Function0<List<CourseRegistration>>() {
            @Override
            public List<CourseRegistration> apply() throws Throwable {
                return find.where().eq("student.id",studentID).findList();
            }
        }).recover(new F.Function<Throwable, List<CourseRegistration>>() {
            @Override
            public List<CourseRegistration> apply(Throwable throwable) throws Throwable {
                play.Logger.error("Problem in fetching course registration",throwable);
                return new ArrayList<CourseRegistration>();
            }
        });
    }


    public  static F.Promise<CourseRegistration> find(final long id){

        return F.Promise.promise(new F.Function0<CourseRegistration>() {
            @Override
            public CourseRegistration apply() throws Throwable {
                return find.byId(id);
            }
        },ctx).recover(new F.Function<Throwable, CourseRegistration>() {
            @Override
            public CourseRegistration apply(Throwable throwable) throws Throwable {


                return new CourseRegistration();
            }
        });
    }




}

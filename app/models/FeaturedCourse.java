package models;

import play.db.ebean.Model;

import javax.persistence.*;

@Entity
public class FeaturedCourse extends Model {
    @Id
    public long id;
    
    @OneToOne
    public Course course;



    public static Model.Finder<Long, FeaturedCourse> find = new Model.Finder<Long, FeaturedCourse>(Long.class, FeaturedCourse.class);

}

package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.Logger;
import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 2/24/15.
 */
@Entity
public class Lecturer extends Model {

    @Id
    public long id;

    public String profile;


    @OneToOne(optional = false,cascade = CascadeType.ALL,fetch = FetchType.EAGER)//(optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",nullable = false)
    @JsonIgnore
    public User user;

    @ManyToMany(cascade = CascadeType.ALL,mappedBy = "lecturers",fetch = FetchType.LAZY)
    public List<Course> courses;

    @ManyToOne
    public Department department;

    @OneToOne(mappedBy = "dean",optional = true,cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public Faculty faculty;



    public static Model.Finder<Long, Lecturer> find = new Model.Finder<Long, Lecturer>(Long.class, Lecturer.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public static F.Promise<List<Lecturer>> find(){
        return F.Promise.promise(new F.Function0<List<Lecturer>>() {
            @Override
            public List<Lecturer> apply() throws Throwable {
                return find.all();
            }
        },ctx).recover(new F.Function<Throwable, List<Lecturer>>() {
            @Override
            public List<Lecturer> apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch Lecturer. DB probably unreachable");

                return new ArrayList<Lecturer>();
            }
        },ctx);
    }

    public static F.Promise<Lecturer> find(final long id){
        return F.Promise.promise(new F.Function0<Lecturer>() {
            @Override
            public Lecturer apply() throws Throwable {

                return find.byId(id);

            }
        },ctx).recover(new F.Function<Throwable, Lecturer>() {
            @Override
            public Lecturer apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch lecturer. DB probably unreachable {}",throwable);
                return new Lecturer();
            }
        },ctx);
    }
}

package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by wale on 5/5/15.
 */
@Entity
public class GeneralAnnouncement extends Model{

    @Id
    public long id;

    public String message;

    @OneToOne(optional = false,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id",nullable = false)
    @JsonIgnore
    public User user;



    public Date issueDate = new Date();




    public static Model.Finder<Long, GeneralAnnouncement> find = new Finder<Long, GeneralAnnouncement>(Long.class, GeneralAnnouncement.class);
}

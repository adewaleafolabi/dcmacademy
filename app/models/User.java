package models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import models.AuditTrails.Login;
import play.Logger;
import play.data.format.Formats;
import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;
import utils.AppException;
import utils.Hash;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Entity
public class User extends Model {

    @Id
    public long id;

    @Formats.NonEmpty
    @Column(unique = true)
    public String username;


    @Formats.NonEmpty
    public String passwordHash;

    @Formats.NonEmpty
    public boolean isEnabled = false;

    @OneToOne(mappedBy = "user",optional = false, cascade = CascadeType.ALL/*, optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL*/)
    public BioData bioData;

    @OneToOne(mappedBy = "user", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    public Lecturer lecturer;

    @OneToOne(mappedBy = "user", optional = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    public Student student;

    @Enumerated(EnumType.STRING)
    public Role role;

    public String confirmationToken;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    public List<Login> loginAuditTrail = new ArrayList<Login>();



    public boolean isValidated;



    /**
     * Confirms an account.
     *
     * @return true if confirmed, false otherwise.
     * @throws AppException App Exception
     */
    public static boolean confirm(User user) throws AppException {
        if (user == null) {
            return false;
        }

        user.confirmationToken = null;
        user.isValidated = true;
        user.isEnabled = true;
        user.save();
        return true;
    }

    /**
     * Enables a user
     */
    public void enableUser(){
        this.isEnabled = true;
        this.save();
    }

    /**
     * Disables a user
     */
    public void disableUser(){
        this.isEnabled = true;
        this.save();
    }

    /**
     * Authenticate a User, from a username and clear password.
     *
     * @param username
     * @param clearPassword clear password
     * @param financialInstitutionCode
     * @return User if authenticated, null otherwise
     * @throws models.utils.AppException App Exception
     */
    public static User authenticate(String username, String clearPassword) throws AppException {




        User user = find.where().eq("username", username).findUnique();

        if(user == null){

            Logger.debug("user not found");
        }


        if (user != null) {

            Logger.debug("User found");

            if (Hash.checkPassword(clearPassword, user.passwordHash)) {

                return user;
            }

            Logger.error("Login authentication failed for user " + username);

        }

        return null;
    }

    /**
     * Changes a user password
     * @param password
     * @throws utils.AppException AppException
     */
    public void changePassword(String password) throws AppException {
        this.passwordHash = Hash.createPassword(password);
        this.save();
    }

    /**
     * Finds a user by his username
     * @param username
     * @return User
     */
    public static User findByUserName(String username){

        return find.where().eq("username",username).findUnique();
    }

    public static User findByEmail(String email){

        return find.where().eq("username",email).findUnique();
    }

    public static Finder<Long, User> find = new Finder<Long, User>(Long.class, User.class);


    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public static F.Promise<List<User>> find(){
        return F.Promise.promise(new F.Function0<List<User>>() {
            @Override
            public List<User> apply() throws Throwable {

                return find.all();

            }
        },ctx).recover(new F.Function<Throwable, List<User>>() {
            @Override
            public List<User> apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch users. DB probably unreachable");

                return new ArrayList<User>();
            }
        },ctx);
    }


    public static F.Promise<List<User>> find(final Map<String,Object> conditions){
        return F.Promise.promise(new F.Function0<List<User>>() {
            @Override
            public List<User> apply() throws Throwable {

                return find.where().allEq(conditions).findList();

            }
        },ctx).recover(new F.Function<Throwable, List<User>>() {
            @Override
            public List<User> apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch users. DB probably unreachable");

                return new ArrayList<User>();
            }
        },ctx);
    }

    public static F.Promise<User> find(final long id){
        return F.Promise.promise(new F.Function0<User>() {
            @Override
            public User apply() throws Throwable {

                return find.byId(id);

            }
        },ctx).recover(new F.Function<Throwable, User>() {
            @Override
            public User apply(Throwable throwable) throws Throwable {
                Logger.info("Unable to fetch user. DB probably unreachable {}",throwable);

                return new User();
            }
        },ctx);
    }




    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, ctx);

    }



    public  static F.Promise<User> findUser(final long id) {


        return F.Promise.promise(new F.Function0<User>() {

            @Override
            public User apply() throws Throwable {

                return find.where().idEq(id).findUnique();

            }


        },ctx).recover(

                new F.Function<Throwable, User>() {

                    @Override
                    public User apply(Throwable t) throws Throwable {

                        Logger.error("Failed to fetch user information.",t);

                        User user = new User();

                        return user;
                    }
                },ctx);

    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", isEnabled=" + isEnabled +
                ", role=" + role +
                '}';
    }
}

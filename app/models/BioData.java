package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;

/**
 * Created by wale on 2/15/15.
 */
@Entity
public class BioData extends Model {

    @Id
    public long id;

    public String salutation;

    public String additionalTitles;

    public String firstName;

    public String middleName;

    public String lastName;

    @Enumerated(EnumType.STRING)
    public Gender gender;

    public String phoneNumber;

    public String residentialAddress;



    @OneToOne(optional = false,cascade = CascadeType.ALL)//(optional = false,fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",nullable = false)
    @JsonIgnore
    public User user;

    public String displayName() {
        return   (this.salutation == null) ? "" : this.salutation + " "
                +((this.firstName == null) ? "" : this.firstName )+ " "
                + ((this.middleName == null) ? "" : this.middleName) + " "
                + ((this.lastName == null) ? "" : this.lastName);
    }


}

package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 5/18/15.
 */
@Entity
public class AcademicPeriod extends Model {

    @Id
    public long id;

    @Column(unique = true)
    public String name;

    public Date startDate;

    public Date endDate;

    public int enrollmentDuration;

    public int gracePeriod;


    @ManyToMany(mappedBy = "academicPeriods")
    @JsonIgnore
    public List<Course> availableCourses;


    public static Model.Finder<Long, AcademicPeriod> find = new Model.Finder<Long, AcademicPeriod>(Long.class, AcademicPeriod.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<AcademicPeriod>> find(){

        return F.Promise.promise(new F.Function0<List<AcademicPeriod>>() {
            @Override
            public List<AcademicPeriod> apply() throws Throwable {
                return find.all();
            }
        });
    }

    public  static F.Promise<AcademicPeriod> find(final long id){

        return F.Promise.promise(new F.Function0<AcademicPeriod>() {
            @Override
            public AcademicPeriod apply() throws Throwable {
                return find.byId(id);
            }
        });
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AcademicPeriod{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append(", enrollmentDuration=").append(enrollmentDuration);
        sb.append(", gracePeriod=").append(gracePeriod);
        sb.append(", availableCourses=").append(availableCourses);
        sb.append('}');
        return sb.toString();
    }
}

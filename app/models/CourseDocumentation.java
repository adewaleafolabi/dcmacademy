package models;

import play.data.format.Formats;
import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 5/4/15.
 */
@Entity
@Table(
        uniqueConstraints=
        @UniqueConstraint(columnNames={"title", "course_id"})
)
public class CourseDocumentation extends Model{

    @Id
    public long id;

    public String title;

    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public Course course;

    public String fileName;

    @Formats.DateTime(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(updatable=false)
    public  java.sql.Timestamp  dateAdded = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());




    public static Model.Finder<Long, CourseDocumentation> find = new Model.Finder<Long, CourseDocumentation>(Long.class, CourseDocumentation.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<CourseDocumentation>> find(){

        return F.Promise.promise(new F.Function0<List<CourseDocumentation>>() {
            @Override
            public List<CourseDocumentation> apply() throws Throwable {
                return find.all();
            }
        });
    }

    public  static F.Promise<CourseDocumentation> find(final long id){

        return F.Promise.promise(new F.Function0<CourseDocumentation>() {
            @Override
            public CourseDocumentation apply() throws Throwable {
                return find.byId(id);
            }
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, ctx);

    }

    public F.Promise<Void> asyncMerge(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                if(id !=0){
                    update();
                }else {
                    save();
                }
                return null;
            }
        }, ctx);

    }
}

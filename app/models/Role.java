package models;

/**
 * Created by wale on 2/15/15.
 */
public enum Role {
    STUDENT,
    LECTURER,
    ADMIN,
    SUPER_ADMIN
}

package models;

/**
 * Created by wale on 5/8/15.
 */
public enum PaymentRoute {
    BANK,
    CASH,
    PAYMENT_GATEWAY

}

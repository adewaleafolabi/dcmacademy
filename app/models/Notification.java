package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 5/5/15.
 */
@Entity
public class Notification extends Model{

    @Id
    public long id;

    public String message;

    @OneToOne(optional = false,cascade = CascadeType.ALL)
    @JoinColumn(name = "lecturer_id",nullable = false)
    @JsonIgnore
    public Lecturer lecturer;


    /*@JoinTable(name = "course_notification", joinColumns = {
            @JoinColumn(name = "notification_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "course_id", referencedColumnName = "id")})
    @ManyToMany(cascade = CascadeType.ALL)*/
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public Course course;


    public Date issueDate = new Date();




    public static Model.Finder<Long, Notification> find = new Model.Finder<Long, Notification>(Long.class, Notification.class);
}

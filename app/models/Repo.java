package models;

import play.Play;

/**
 * Created by wale on 5/4/15.
 */
public enum Repo{


    LECTURER (Play.application().configuration().getString("app.image.storage") +"/LECTURER"),
    COURSE (Play.application().configuration().getString("app.image.storage") +"/COURSE"),
    STUDENT (Play.application().configuration().getString("app.image.storage") +"/STUDENT"),
    FACULTY (Play.application().configuration().getString("app.image.storage") +"/FACULTY"),
    DEPARTMENT (Play.application().configuration().getString("app.image.storage") +"/DEPARTMENT"),
    GENERAL (Play.application().configuration().getString("app.image.storage") +"/GENERAL"),
    DOCUMENTS(Play.application().configuration().getString("app.image.storage") +"/DOCUMENTS"),
    TIME_TABLE(Play.application().configuration().getString("app.image.storage") +"/DOCUMENTS/TIME_TABLE");

    private String repoPath;

    Repo(String repoPath) {
        this.repoPath = repoPath;
    }

    public String getRepoPath() {
        return repoPath;
    }




}
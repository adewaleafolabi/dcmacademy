package models;

import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 5/8/15.
 */
@Entity
public class FeePayment extends Model{
    
    @Id
    public long id;

    @Enumerated(EnumType.STRING)
    public PaymentRoute paymentMethod;
    
    public long amount;

    @OneToOne(optional = false,cascade = CascadeType.ALL)
    public Course course;



    @OneToOne
    public AcademicPeriod academicPeriod;

    public String paymentReference;
    
    public Date paymentDate;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Student student;

    public static Model.Finder<Long, FeePayment> find = new Model.Finder<Long, FeePayment>(Long.class, FeePayment.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<FeePayment>> find(){

        return F.Promise.promise(new F.Function0<List<FeePayment>>() {
            @Override
            public List<FeePayment> apply() throws Throwable {
                return find.all();
            }
        });
    }

    public  static F.Promise<FeePayment> find(final long id){

        return F.Promise.promise(new F.Function0<FeePayment>() {
            @Override
            public FeePayment apply() throws Throwable {
                return find.byId(id);
            }
        });
    }
    
    
    
    
}

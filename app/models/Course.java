package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 2/24/15.
 */
@Entity
public class Course extends Model {
    @Id
    public long id;

    public String title;

    @Lob
    public String outline;

    @Lob
    public String objective;

    public int duration;

    @Column(nullable = true)
    public long fee =0L;


    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public List<Notification> notifications;


    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public List<CourseRegistration> registrations;

    @ManyToOne(cascade = CascadeType.ALL)
    public Department department;

    @ManyToMany(cascade = CascadeType.ALL)
    public List<AcademicPeriod> academicPeriods;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public List<Lecturer> lecturers;

    @OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public List<CourseDocumentation> courseDocumentations;


    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public List<Student> students;


    @ManyToMany(mappedBy = "recentlyViewedCourses")
    @JsonIgnore
    public List<Student> recentlyViewedStudents ;

    public static Model.Finder<Long, Course> find = new Model.Finder<Long, Course>(Long.class, Course.class);



    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<Course>> find(){

        return F.Promise.promise(new F.Function0<List<Course>>() {
            @Override
            public List<Course> apply() throws Throwable {
                return find.all();
            }
        }).recover(new F.Function<Throwable, List<Course>>() {
            @Override
            public List<Course> apply(Throwable throwable) throws Throwable {
                play.Logger.error("Problem in fetching courses",throwable);
                return new ArrayList<Course>();
            }
        });
    }

    public  static F.Promise<Course> find(final long id){

        return F.Promise.promise(new F.Function0<Course>() {
            @Override
            public Course apply() throws Throwable {
                return find.byId(id);
            }
        },ctx).recover(new F.Function<Throwable, Course>() {
            @Override
            public Course apply(Throwable throwable) throws Throwable {
                Course c = new Course();
                c.title = "Coming Soon";
                c.objective ="Coming Soon";
                c.duration=0;

                Department d = new Department();
                d.code="";
                d.name="";
                Faculty f = new Faculty();
                f.code="";
                f.name="";
                d.faculty=f;
                c.department= d;

                return c;
            }
        });
    }


    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, ctx);

    }


    public F.Promise<Void> asyncUpdate(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                 update();
                return null;
            }
        }, ctx);

    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                "fee=" + fee +
                ", title='" + title + '\'' +
                ", outline='" + outline + '\'' +
                ", objective='" + objective + '\'' +
                ", duration=" + duration +
                ", department=" + department.id +
                '}';
    }
}

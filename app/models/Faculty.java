package models;

import play.db.ebean.Model;
import play.libs.Akka;
import play.libs.F;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 2/25/15.
 */
@Entity
public class Faculty extends Model {

    @Id
    public long id;

    @Column(unique = true)
    public String code;

    @Column(unique = true)
    public String name;

    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public List<Department> departments;


    @OneToOne(cascade = CascadeType.ALL)
    public Lecturer dean;


    public static Model.Finder<Long, Faculty> find = new Model.Finder<>(Long.class, Faculty.class);

    private static scala.concurrent.ExecutionContext ctx = Akka.system().dispatchers().lookup("akka.db-dispatcher");

    public  static F.Promise<List<Faculty>> find(){


        return F.Promise.promise(new F.Function0<List<Faculty>>() {
            @Override
            public List<Faculty> apply() throws Throwable {
                return find.all();
            }
        },ctx).recover(new F.Function<Throwable, List<Faculty>>() {
            @Override
            public List<Faculty> apply(Throwable throwable) throws Throwable {

                play.Logger.error("Failed to fetch faculties from db {}",throwable);

                return new ArrayList<Faculty>();
            }
        },ctx);
    }

    public  static F.Promise<Faculty> find(final long id){


        return F.Promise.promise(new F.Function0<Faculty>() {
            @Override
            public Faculty apply() throws Throwable {
                return find.byId(id);
            }
        },ctx).recover(new F.Function<Throwable, Faculty>() {
            @Override
            public Faculty apply(Throwable throwable) throws Throwable {

                play.Logger.error("Failed to fetch faculty from db {}",throwable);

                return new Faculty();
            }
        },ctx);
    }

    public F.Promise<Void> asyncSave(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                save();
                return null;
            }
        }, ctx);

    }

    public F.Promise<Void> asyncMerge(){

        return F.Promise.promise(new F.Function0<Void>() {
            @Override
            public Void apply() throws Throwable {
                if(id !=0){
                    update();
                }else {
                    save();
                }
                return null;
            }
        }, ctx);

    }

}

package models;

import play.db.ebean.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by wale on 5/11/15.
 */
@Entity
public class ExamRecord extends Model{
    @Id
    public long id;


    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public Student student;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public Course course;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    public AcademicPeriod academicPeriod;


    public int caScore;

    public int attendanceScore;

    public int examScore;

    public Date recordDate = new Date();


    public static Finder<Long, ExamRecord> find = new Finder<Long, ExamRecord>(Long.class, ExamRecord.class);

}

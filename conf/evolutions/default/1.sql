# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table bio_data (
  id                        bigint auto_increment not null,
  first_name                varchar(255),
  middle_name               varchar(255),
  last_name                 varchar(255),
  gender                    varchar(1),
  phone_number              varchar(255),
  residential_address       varchar(255),
  user_id                   bigint not null,
  constraint ck_bio_data_gender check (gender in ('M','F')),
  constraint pk_bio_data primary key (id))
;

create table student (
  role                      varchar(8),
  student_number            varchar(255),
  user_id                   bigint not null,
  constraint ck_student_role check (role in ('STUDENT','LECTURER','ADMIN')))
;

create table user (
  id                        bigint auto_increment not null,
  username                  varchar(255),
  password_hash             varchar(255),
  is_enabled                tinyint(1) default 0,
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id))
;

alter table bio_data add constraint fk_bio_data_user_1 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_bio_data_user_1 on bio_data (user_id);
alter table student add constraint fk_student_user_2 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_student_user_2 on student (user_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table bio_data;

drop table student;

drop table user;

SET FOREIGN_KEY_CHECKS=1;


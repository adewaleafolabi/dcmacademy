name := """DCMACADEMY"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "org.mindrot" % "jbcrypt" % "0.3m",
  "mysql" % "mysql-connector-java" % "5.1.18",
  "org.apache.commons" % "commons-lang3" % "3.3.2",
  "org.jasypt" % "jasypt" % "1.9.2",
  "org.apache.commons" % "commons-collections4" % "4.0",
  "org.apache.poi" % "poi" % "3.11",
  "com.typesafe.play.plugins" %% "play-plugins-mailer" % "2.3.1",
  "commons-io" % "commons-io" % "2.3"
)

resolvers ++= Seq(
  "Apache" at "http://repo1.maven.org/maven2/",
  "jBCrypt Repository" at "http://repo1.maven.org/maven2/org/",
  "Sonatype OSS Snasphots" at "http://oss.sonatype.org/content/repositories/snapshots"
)


offline:=true
